% check how pre-post can have such a differential relation to behavior (YA only for now)

%% extract values from v1

pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/D_PLS_Dim/';
pn.v1 = [pn.root, 'B_data/SD_STSWD_byCond/'];
pn.v2 = [pn.root, 'B_data/SD_STSWD_byCond_v2/'];


IDs = {'1117';'1118';'1120';'1124';'1125';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';'1215';...
    '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281'};

MergedVersion1 = []; MergedVersion2 = [];
for indID = 1:numel(IDs)
    tmp1 = load([pn.v1, 'SD_',IDs{indID},'_BfMRIsessiondata.mat']);
    tmp2 = load([pn.v2, 'SD_',IDs{indID},'_v2_BfMRIsessiondata.mat']);
    MergedVersion1(indID,:,:) = tmp1.st_datamat([1:4,7],:);
    MergedVersion2(indID,:,:) = tmp2.st_datamat([1:4,7],:);
end

size(MergedVersion1)

figure; 
subplot(1,2,1); imagesc(squeeze(MergedVersion1(:,5,:))); title('Contrast 4-1 before correction')
subplot(1,2,2); imagesc(squeeze(MergedVersion2(:,5,:))); title('Contrast 4-1 after correction')

figure; 
subplot(1,2,1); imagesc(squeeze(MergedVersion1(:,1,:))); title('L1 before correction')
subplot(1,2,2); imagesc(squeeze(MergedVersion2(:,1,:))); title('L1 after correction')

figure; 
subplot(3,2,1); imagesc(squeeze(MergedVersion1(:,2,:)-MergedVersion1(:,1,:)), [-1 1]); title('L1 before correction')
subplot(3,2,2); imagesc(squeeze(MergedVersion2(:,2,:)-MergedVersion2(:,1,:)), [-1 1]); title('L1 after correction')
subplot(3,2,3); imagesc(squeeze(MergedVersion1(:,3,:)-MergedVersion1(:,1,:)), [-1 1]); title('L1 before correction')
subplot(3,2,4); imagesc(squeeze(MergedVersion2(:,3,:)-MergedVersion2(:,1,:)), [-1 1]); title('L1 after correction')
subplot(3,2,5); imagesc(squeeze(MergedVersion1(:,4,:)-MergedVersion1(:,1,:)), [-1 1]); title('L1 before correction')
subplot(3,2,6); imagesc(squeeze(MergedVersion2(:,4,:)-MergedVersion2(:,1,:)), [-1 1]); title('L1 after correction')

figure; 
subplot(3,2,1); imagesc(squeeze(MergedVersion1(:,3,:)-MergedVersion1(:,2,:)), [-1 1]); title('L1 before correction')
subplot(3,2,2); imagesc(squeeze(MergedVersion2(:,3,:)-MergedVersion2(:,2,:)), [-1 1]); title('L1 after correction')
subplot(3,2,3); imagesc(squeeze(MergedVersion1(:,4,:)-MergedVersion1(:,2,:)), [-1 1]); title('L1 before correction')
subplot(3,2,4); imagesc(squeeze(MergedVersion2(:,4,:)-MergedVersion2(:,2,:)), [-1 1]); title('L1 after correction')

x1 = squeeze(nanmean(MergedVersion1(:,4,:)-MergedVersion1(:,1,:),3));
x2 = squeeze(nanmean(MergedVersion2(:,4,:)-MergedVersion2(:,1,:),3));
figure; scatter(x1, x2, 'filled')

% mask with coarse thalamus
BASEPATH    = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/';
addpath(genpath([BASEPATH,'analyses/B_PLS/T_tools/NIFTI_toolbox/']));
thalMask = load_nii('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/D_PLS_Dim/B_data/SD_STSWD_byCond/Z_archive/CoarseThalamus.nii');

VoxIdx = find(thalMask.img==1);
[a] = ismember(tmp2.st_coords,VoxIdx);

x1 = squeeze(nanmean(MergedVersion1(:,4,a)-MergedVersion1(:,1,a),3));
x2 = squeeze(nanmean(MergedVersion2(:,4,a)-MergedVersion2(:,1,a),3));

x1 = squeeze(nanmean(MergedVersion1(:,4,a)-MergedVersion1(:,2,a),3));
x2 = squeeze(nanmean(MergedVersion2(:,4,a)-MergedVersion2(:,2,a),3));
figure; scatter(x1, x2, 'filled')

%% correlate with changes in NDT

dataPath = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/D_DDM/B_data/';
csvDataMRI = csvread([dataPath,'StateSwitchDynamicTrialData_MRI.dat'],2,0);
IDsMRI = cellstr(num2str(unique(csvDataMRI(:,7))));
numSubs = numel(find(ismember(IDsMRI,IDs)));
for indID = 1:numel(IDs)
    curID = find(strcmp(IDsMRI, IDs{indID}));
    NDTchange(indID) = nanmean(MeanValuesMRI(2, 2:4, curID),2)-nanmean(MeanValuesMRI(2, 1, curID),2);
end

cond1 = 4;
cond2 = 1;
for indID = 1:numel(IDs)
    curID = find(strcmp(IDsMRI, IDs{indID}));
    NDTchange(indID) = nanmean(MeanValuesMRI(2, cond1, curID),2)./nanmean(MeanValuesMRI(2, cond2, curID),2);
end
x1 = squeeze(nanmean(nanmean(MergedVersion1(:,cond1,a),2)./nanmean(MergedVersion1(:,cond2,a),2),3));
x2 = squeeze(nanmean(nanmean(MergedVersion2(:,cond1,a),2)./nanmean(MergedVersion2(:,cond2,a),2),3));
y = NDTchange;
figure; 
subplot(1,3,1); scatter(x1, y, 'filled')
subplot(1,3,2); scatter(x2, y, 'filled')
subplot(1,3,3); scatter(x1, x2, 'filled')

[r,p] = corrcoef(x1, y)
[r,p] = corrcoef(x2, y)
[r,p] = corrcoef(x1, x2)

figure; imagesc(corrcoef([nanmean(MergedVersion1(:,:,a),3), nanmean(MergedVersion2(:,:,a),3)]))

%% relative change

cond1 = 2:4;
cond2 = 1;
for indID = 1:numel(IDs)
    curID = find(strcmp(IDsMRI, IDs{indID}));
    NDTchange(indID) = (nanmean(MeanValuesMRI(2, cond1, curID),2)-nanmean(MeanValuesMRI(2, cond2, curID),2))./nanmean(MeanValuesMRI(2, cond2, curID),2);
end
x1 = squeeze(nanmean((nanmean(MergedVersion1(:,cond1,a),2)-nanmean(MergedVersion1(:,cond2,a),2))./nanmean(MergedVersion1(:,cond2,a),2),3));
x2 = squeeze(nanmean((nanmean(MergedVersion2(:,cond1,a),2)-nanmean(MergedVersion1(:,cond2,a),2))./nanmean(MergedVersion2(:,cond2,a),2),3));
y = NDTchange;
figure; 
subplot(1,3,1); scatter(x1, y, 'filled')
subplot(1,3,2); scatter(x2, y, 'filled')
subplot(1,3,3); scatter(x1, x2, 'filled')

[r,p] = corrcoef(x1, y)
[r,p] = corrcoef(x2, y)
[r,p] = corrcoef(x1, x2)