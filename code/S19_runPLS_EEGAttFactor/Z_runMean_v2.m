restoredefaultpath
clear all; clc;

% pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/';
% pn.plsroot      = [pn.root, 'analyses/D_PLS_Dim/'];
% pn.plstoolbox   = [pn.plsroot, 'T_tools/Z_archive/pls/']; addpath(genpath(pn.plstoolbox));
% pn.plsdir       = [pn.plsroot, 'B_data/SD_STSWD_byCond_v4/'];

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/';
pn.plstoolbox   = [pn.root, 'D2_PLS_VarTbx/T_tools/PLS_LNDG2018/Pls/']; addpath(genpath(pn.plstoolbox));
pn.plsdir       = [pn.root, 'D_PLS_Dim/B_data/Mean_v2/'];

cd(pn.plsdir);

batch_plsgui('behavPLS_STSWD_N42_YAonly_SDBOLD_EEGAttFactor_3mm_1000P1000B_BfMRIanalysis.txt')

plsgui
