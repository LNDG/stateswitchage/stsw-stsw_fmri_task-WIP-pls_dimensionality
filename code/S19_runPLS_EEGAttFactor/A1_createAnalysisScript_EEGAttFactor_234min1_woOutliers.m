% Create info file for PLS analysis

%% get EEG-based attention factor

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S14_Gamma/B_data/Z2_EEGAttentionFactor_YAOA.mat', 'EEGAttentionFactor')

%% YA:

clearvars -except EEGAttentionFactor

% N = 44 YA, 1151,1252,1228,1247 excluded
IDs = {'1117';'1118';'1120';'1124';'1125';'1126';'1131';'1132';'1135';'1136';...
    '1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';'1215';...
    '1216';'1219';'1223';'1227';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1250';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281'};

numSubs = numel(find(ismember(EEGAttentionFactor.IDs,IDs)));
IDs = IDs(ismember(IDs,EEGAttentionFactor.IDs));
for indCond = 1
    for indID = 1:numel(IDs)
        disp(['Processing ID ', IDs{indID}])
        groupfiles{indID,1} = ['SD_',IDs{indID}, '_v2_BfMRIsessiondata.mat'];
        groupfiles_mean{indID,1} = ['task_',IDs{indID}, '_BfMRIsessiondata.mat'];
        curID = find(strcmp(EEGAttentionFactor.IDs, IDs{indID}));
        dataForPLS{(indCond-1)*numSubs+indID,1} = EEGAttentionFactor.PCAalphaGamma(curID);
        dataname{(indCond-1)*numSubs+indID,1} = 'behavior_data';
    end
end
groupfiles = groupfiles(find(~cellfun(@isempty,groupfiles)));
groupfiles_mean = groupfiles_mean(find(~cellfun(@isempty,groupfiles_mean)));

Fillin.GROUPFILES = groupfiles';
Fillin.GROUPFILES_Mean = groupfiles_mean';
Fillin.DATANAME = dataname;
Fillin.VALUE = dataForPLS;

% Output necessary information, has to be manually copied into .txt file

clc;
for indSub = 1:numel(IDs) 
    fprintf('%s ', Fillin.GROUPFILES{indSub});
end

clc;
for indSub = 1:numel(IDs) 
    fprintf('%s ', Fillin.GROUPFILES_Mean{indSub});
end

clc;
for indSub = 1:size(Fillin.VALUE,1) 
    fprintf('%s    %d \n', Fillin.DATANAME{indSub}, Fillin.VALUE{indSub,1});
end

%% OAs:

clearvars -except EEGAttentionFactor

%N = 53 OAs, 2157 removed
IDs = {'2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';...
    '2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';'2149';...
    '2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';...
    '2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2237';'2238';...
    '2241';'2244';'2246';'2248';'2250';'2251';'2252';'2258';'2261'};

numSubs = numel(find(ismember(EEGAttentionFactor.IDs,IDs)));
IDs = IDs(ismember(IDs,EEGAttentionFactor.IDs));
for indCond = 1
    for indID = 1:numel(IDs)
        disp(['Processing ID ', IDs{indID}])
        groupfiles{indID,1} = ['SD_',IDs{indID}, '_v2_BfMRIsessiondata.mat'];
        groupfiles_mean{indID,1} = ['task_',IDs{indID}, '_BfMRIsessiondata.mat'];
        curID = find(strcmp(EEGAttentionFactor.IDs, IDs{indID}));
        dataForPLS{(indCond-1)*numSubs+indID,1} = EEGAttentionFactor.PCAalphaGamma(curID);
        dataname{(indCond-1)*numSubs+indID,1} = 'behavior_data';
    end
end
groupfiles = groupfiles(find(~cellfun(@isempty,groupfiles)));
groupfiles_mean = groupfiles_mean(find(~cellfun(@isempty,groupfiles_mean)));

Fillin.GROUPFILES = groupfiles';
Fillin.GROUPFILES_Mean = groupfiles_mean';
Fillin.DATANAME = dataname;
Fillin.VALUE = dataForPLS;

% Output necessary information, has to be manually copied into .txt file

clc;
for indSub = 1:numel(IDs) 
    fprintf('%s ', Fillin.GROUPFILES{indSub});
end

clc;
for indSub = 1:numel(IDs) 
    fprintf('%s ', Fillin.GROUPFILES_Mean{indSub});
end

clc;
for indSub = 1:size(Fillin.VALUE,1) 
    fprintf('%s    %d \n', Fillin.DATANAME{indSub}, Fillin.VALUE{indSub,1});
end
