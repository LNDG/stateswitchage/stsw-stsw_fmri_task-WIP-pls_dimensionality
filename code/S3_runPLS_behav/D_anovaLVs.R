rm(list = ls()) # clear workspace

install.packages('R.matlab')

library(R.matlab)

# read the data for the ANOVA from MATLAB output

data = readMat('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/D_PLS_Dim/B_data/SD_Dim_ByCond_forR.mat')
data = as.data.frame(matrix(unlist(data), nrow=7, byrow=T),stringsAsFactors=FALSE, header = F)
data = t(data) # transpose matrix
data = data.frame(data) # convert to data frame
names(data) = c("ID", "age", "condition", "LV1valueBrain", "LV2valueBrain", "LV1valueDim", "LV2valueDim")

data$ID = as.factor(data$ID)
data$age = as.factor(data$age)
data$condition = as.factor(data$condition)
data$LV1valueBrain = as.numeric(sub(",", ".", data$LV1valueBrain, fixed = TRUE))
data$LV2valueBrain = as.numeric(sub(",", ".", data$LV2valueBrain, fixed = TRUE))
data$LV1valueDim = as.numeric(sub(",", ".", data$LV1valueDim, fixed = TRUE))
data$LV2valueDim = as.numeric(sub(",", ".", data$LV2valueDim, fixed = TRUE))

# repeated measures anova (i.e. mixed-design) (http://www.cookbook-r.com/Statistical_analysis/ANOVA/)

# 2x4 mixed:
# IV between: age
# IV within:  condition
# DV:         value

# Note that ID has to be a factor.

aov_age_cond <- aov(LV1valueBrain ~ age*condition + Error(ID/condition), data=data)
summary(aov_age_cond)
model.tables(aov_age_cond, "means")

# There is a main effect of age, but no effect of condition.

aov_age_cond <- aov(LV2valueBrain ~ age*condition + Error(ID/condition), data=data)
summary(aov_age_cond)
model.tables(aov_age_cond, "means")

aov_age_cond <- aov(LV1valueDim ~ age*condition + Error(ID/condition), data=data)
summary(aov_age_cond)
model.tables(aov_age_cond, "means")

aov_age_cond <- aov(LV2valueDim ~ age*condition + Error(ID/condition), data=data)
summary(aov_age_cond)
model.tables(aov_age_cond, "means")

# There is a strong effect of condition on task dimensionality LV, but the patterns appear a bit weird.