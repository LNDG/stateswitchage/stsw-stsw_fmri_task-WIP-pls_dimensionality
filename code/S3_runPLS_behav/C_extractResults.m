figure; imagesc(result.usc)
figure; imagesc(result.vsc)

%% sort by age (based on IDs)

% N = 43 YA + 53 OA; 1124 removed
IDs = {'1117';'1118';'1120';'1125';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';'1215';...
    '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';...
    '2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';'2149';'2157';...
    '2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';...
    '2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2237';'2238';...
    '2241';'2244';'2246';'2248';'2250';'2251';'2252';'2258';'2261'};

indCount = 1;
for indCond = 1:4
    for indID = 1:numel(IDs)
        group(indCount) = str2num(IDs{indID}(1));
        cond(indCount) = indCond;
        indCount = indCount+1;
    end
end

figure; 
subplot(1,2,1);
    imagesc([result.usc(group==1,:);result.usc(group==2,:)], [-200 200]); % voxel LV
    title('Voxel LV'); xlabel('LVs');
subplot(1,2,2);
    imagesc([result.vsc(group==1,:);result.vsc(group==2,:)]); % behavior LV
    title('Behavior LV'); xlabel('LVs');

%% convert data to long format for export to R

% subject no. | age group | condition | LV1 value brain | LV2 value brain | LV1 value dim | LV2 value dim

indCount = 1;
dataMat = cell(1);
for indCond = 1:4
    for indID = 1:numel(IDs)
        dataMat{indCount,1} = IDs{indID};
        if str2num(IDs{indID}(1)) == 1
            dataMat{indCount,2} = 'young';
        else dataMat{indCount,2} = 'old';
        end
        dataMat{indCount,3} = indCond;
        dataMat{indCount,4} = result.usc(indCount,1);
        dataMat{indCount,5} = result.usc(indCount,2);
        dataMat{indCount,6} = result.vsc(indCount,1);
        dataMat{indCount,7} = result.vsc(indCount,2);
        indCount = indCount+1;
    end
end

pn.savePath = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/D_PLS_Dim/B_data/';

save([pn.savePath, 'SD_Dim_ByCond_forR.mat'], 'dataMat');