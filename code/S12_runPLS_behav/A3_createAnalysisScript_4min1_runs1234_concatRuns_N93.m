% Create info file for PLS analysis

%% get behavioral data

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/A_MergeIndividualData/B_data/SummaryData_N102_byRun.mat')

for indID = 1:size(SummaryData.MRI.logRTs_md,1)
    y = squeeze(nanmean(SummaryData.MRI.logRTs_mean(indID,:,:,:),3)); % average across attributes
    Dim4minDim1_RT_MRI(indID,:) = (nanmean(y(:,4),2)-y(:,1))./y(:,1);
end
Dim4minDim1_RT_MRI(isnan(Dim4minDim1_RT_MRI)) = 0;
sum(sum(corrcoef(Dim4minDim1_RT_MRI)))

%% YA:

clearvars -except Dim4minDim1_RT_MRI IDs_all

% N = 42 YA; 1228 removed (outlier) 1227 removed (outlier); 1234 removed (no behav data R1)
IDs = {'1117';'1118';'1120';'1124';'1125';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';'1215';...
    '1216';'1219';'1223';'1233';'1237';'1239';'1240';'1243';...
    '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281'};

numSubs = numel(find(ismember(IDs_all,IDs)));
for indCond = 1:4
    for indID = 1:numel(IDs)
        disp(['Processing ID ', IDs{indID}])
        groupfiles{indID,1} = ['SD_',IDs{indID}, '_v5_runsConcat_BfMRIsessiondata.mat'];
        curID = find(strcmp(IDs_all, IDs{indID}));
        dataForPLS{(indCond-1)*numSubs+indID,1} = Dim4minDim1_RT_MRI(curID,indCond);
        dataname{(indCond-1)*numSubs+indID,1} = 'behavior_data';
    end
end
groupfiles = groupfiles(find(~cellfun(@isempty,groupfiles)));

Fillin.GROUPFILES = groupfiles';
Fillin.DATANAME = dataname;
Fillin.VALUE = dataForPLS;

% Output necessary information, has to be manually copied into .txt file

clc;
for indSub = 1:numel(IDs) 
    fprintf('%s ', Fillin.GROUPFILES{indSub});
end

clc;
for indSub = 1:size(Fillin.VALUE,1) 
    fprintf('%s    %d\n', ...
        Fillin.DATANAME{indSub}, Fillin.VALUE{indSub,1});
end

%% OAs:

clearvars -except Dim4minDim1_RT_MRI IDs_all

%N = 53 OAs, 2112, 2120 removed (behav outliers)
IDs = {'2104';'2107';'2108';'2118';'2121';'2123';'2125';'2129';'2130';...
    '2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';'2149';'2157';...
    '2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';...
    '2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2238';...
    '2241';'2244';'2246';'2248';'2250';'2251';'2252';'2258';'2261'};

numSubs = numel(find(ismember(IDs_all,IDs)));
for indCond = 1:4
    for indID = 1:numel(IDs)
        disp(['Processing ID ', IDs{indID}])
        groupfiles{indID,1} = ['SD_',IDs{indID}, '_v5_runsConcat_BfMRIsessiondata.mat'];
        curID = find(strcmp(IDs_all, IDs{indID}));
        dataForPLS{(indCond-1)*numSubs+indID,1} = Dim4minDim1_RT_MRI(curID,indCond);
        dataname{(indCond-1)*numSubs+indID,1} = 'behavior_data';
    end
end
groupfiles = groupfiles(find(~cellfun(@isempty,groupfiles)));

Fillin.GROUPFILES = groupfiles';
Fillin.DATANAME = dataname;
Fillin.VALUE = dataForPLS;

% Output necessary information, has to be manually copied into .txt file

clc;
for indSub = 1:numel(IDs) 
    fprintf('%s ', Fillin.GROUPFILES{indSub});
end

clc;
for indSub = 1:size(Fillin.VALUE,1) 
    fprintf('%s    %d\n', ...
        Fillin.DATANAME{indSub}, Fillin.VALUE{indSub,1});
end
