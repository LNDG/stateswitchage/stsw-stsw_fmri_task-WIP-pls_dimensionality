restoredefaultpath
clear all; clc;

% pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/';
% pn.plsroot      = [pn.root, 'analyses/D_PLS_Dim/'];
% pn.plstoolbox   = [pn.plsroot, 'T_tools/Z_archive/pls/']; addpath(genpath(pn.plstoolbox));
% pn.plsdir       = [pn.plsroot, 'B_data/SD_STSWD_byCond_v4/'];

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/';
pn.plstoolbox   = [pn.root, 'D2_PLS_VarTbx/T_tools/PLS_LNDG2018/Pls/']; addpath(genpath(pn.plstoolbox));
pn.plsdir       = [pn.root, 'D_PLS_Dim/B_data/SD_STSWD_byCond_v2/'];

cd(pn.plsdir);

batch_plsgui('behavPLS_STSWD_N97_SDBOLD_DDM4min1_vmintmulta_3mm_1000P1000B_BfMRIanalysis.txt')
batch_plsgui('behavPLS_STSWD_N42_YAonly_SDBOLD_EEGAttFactor_3mm_1000P1000B_BfMRIanalysis.txt')
batch_plsgui('behavPLS_STSWD_N38_YAonly_SDBOLD_EEGAttFactor_3mm_1000P1000B_BfMRIanalysis.txt')
batch_plsgui('behavPLS_STSWD_N93_SDBOLD_EEGAttFactor_3mm_1000P1000B_BfMRIanalysis.txt')
batch_plsgui('behavPLS_STSWD_N92_SDBOLD_EEGAttFactor_3mm_1000P1000B_BfMRIanalysis.txt')
batch_plsgui('behavPLS_STSWD_N92_SDBOLD_EEGAttFactor_wTAG_3mm_1000P1000B_BfMRIanalysis.txt')

plsgui
