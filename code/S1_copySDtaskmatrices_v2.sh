
% copy all SD files

sourceDir="/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B_PLS/B_data/SD_STSWD_task_v1"
targetDir="/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/D_PLS_Dim/B_data/SD_STSWD_byCond_v2"
cd ${sourceDir}
#for file in SD_*_BfMRIsessiondata.mat ; do echo cp "$file" "${targetDir}/${file}";done
for file in SD_*_v2_BfMRIsessiondata.mat ; do cp "$file" "${targetDir}/${file}";done

% copy all mean BOLD files

for file in task_*_BfMRIsessiondata.mat ; do cp "$file" "${targetDir}/${file}";done
