% This script extracts the necessary infos for a PLS analysis of
% dimensionality. Unfortunately, text files are hard to edit, so I rely on
% manual editing in the end. The necessary information is output in the
% command window and can be directly copied to replace the placeholders.

% 180202 | written by JQK
% 180228 | adpated for STSWD YA from Merlin2 phycaa+
% 180302 | adapted for STSWD task
% 180403 | split by age group

% SDBOLD_BehavPLS_N94_SDDim1234_logRhythmPow1234_3mm_1000P1000B_BfMRIanalysis

clear all; clc;

%% process info for YAs

% N = 43 YA; 1124 removed
IDs = {'1117';'1118';'1120'; '1125';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';'1215';...
    '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281'};

% load alpha and slope values
pn.alphaSlope = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S5B_eBOSC_CSD/B_data/';
load([pn.alphaSlope, 'E_InterceptsSlopelogAlpha.mat'], 'BetweenChannel*', 'info');

% load dim values
pn.dimFiles = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/D_PLS_Dim/B_data/SpatialDim/';
DimData = load([pn.dimFiles, 'N181_PCAcorr_dimensionality_spatial.mat']);

% create complete ID cell

IntersectsAlphaDim = intersect(DimData.IDs, info.IDs);
IntersectsAll = intersect(IntersectsAlphaDim, IDs);

IDs = IntersectsAll; % remove IDs that were not shared across modalities

% PLS behavior is stacked by: group, condition, subject

numConds = 4;
numSubs = numel(IDs);
for indCond = 1:numConds
for indID = 1:numSubs
    disp(['Processing ID ', IDs{indID}])
    groupfiles{indID,1} = ['SD_',IDs{indID}, '_diffs_BfMRIsessiondata.mat'];
    groupfiles_mean{indID,1} = ['task_',IDs{indID}, '_BfMRIsessiondata.mat'];
    % add rhythm condition info
    rhythmSubject = find(strcmp(info.IDs, IDs{indID}));
    dataForPLS{(indCond-1)*numSubs+indID,1} = BetweenChannelMdTotal(rhythmSubject,indCond);
    % add dimensionality
    dimSubject = ismember(DimData.IDs,IDs{indID});
    dataForPLS{(indCond-1)*numSubs+indID,2} = DimData.dimensionality(dimSubject,indCond);
    dataname{(indCond-1)*numSubs+indID,1} = 'behavior_data';
end
end
groupfiles = groupfiles(find(~cellfun(@isempty,groupfiles)));
groupfiles_mean = groupfiles_mean(find(~cellfun(@isempty,groupfiles_mean)));

% prepare ouput for PLS file

Fillin.GROUPFILES = groupfiles';
Fillin.GROUPFILES_Mean = groupfiles_mean';
Fillin.DATANAME = dataname;
Fillin.VALUE = dataForPLS;

for indSub = 1:numel(groupfiles)
    fprintf('%s ', Fillin.GROUPFILES{indSub});
end

for indSub = 1:numel(groupfiles_mean)
    fprintf('%s ', Fillin.GROUPFILES_Mean{indSub});
end

for indSub = 1:size(Fillin.VALUE,1)
    fprintf('%s    %d %d\n', Fillin.DATANAME{indSub}, Fillin.VALUE{indSub, 1},...
        Fillin.VALUE{indSub, 2});
end

for indSub = 1:size(Fillin.VALUE,1)
    fprintf('%s    %d\n', Fillin.DATANAME{indSub}, Fillin.VALUE{indSub, 1});
end

figure; scatter([Fillin.VALUE{:, 1}], [Fillin.VALUE{:, 2}])

%% process info for OAs

clear all; clc;

% N = 53 OA
IDs = {'2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';...
    '2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';'2149';'2157';...
    '2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';...
    '2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2237';'2238';...
    '2241';'2244';'2246';'2248';'2250';'2251';'2252';'2258';'2261'};

% load alpha and slope values
pn.alphaSlope = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S5B_eBOSC_CSD/B_data/';
load([pn.alphaSlope, 'E_InterceptsSlopelogAlpha_OA.mat'], 'BetweenChannel*', 'info');

% load dim values
pn.dimFiles = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/D_PLS_Dim/B_data/SpatialDim/';
DimData = load([pn.dimFiles, 'N181_PCAcorr_dimensionality_spatial.mat']);

% create complete ID cell

IntersectsAlphaDim = intersect(DimData.IDs, info.IDs);
IntersectsAll = intersect(IntersectsAlphaDim, IDs);

IDs = IntersectsAll; % remove IDs that were not shared across modalities

% PLS behavior is stacked by: group, condition, subject

numConds = 4;
numSubs = numel(IDs);
for indCond = 1:numConds
for indID = 1:numSubs
    disp(['Processing ID ', IDs{indID}])
    groupfiles{indID,1} = ['SD_',IDs{indID}, '_diffs_BfMRIsessiondata.mat'];
    groupfiles_mean{indID,1} = ['task_',IDs{indID}, '_BfMRIsessiondata.mat'];
    % add rhythm condition info
    rhythmSubject = find(strcmp(info.IDs, IDs{indID}));
    dataForPLS{(indCond-1)*numSubs+indID,1} = BetweenChannelMdTotal(rhythmSubject,indCond);
    % add dimensionality
    dimSubject = ismember(DimData.IDs,IDs{indID});
    dataForPLS{(indCond-1)*numSubs+indID,2} = DimData.dimensionality(dimSubject,indCond);
    dataname{(indCond-1)*numSubs+indID,1} = 'behavior_data';
end
end
groupfiles = groupfiles(find(~cellfun(@isempty,groupfiles)));
groupfiles_mean = groupfiles_mean(find(~cellfun(@isempty,groupfiles_mean)));

% prepare ouput for PLS file

Fillin.GROUPFILES = groupfiles';
Fillin.GROUPFILES_Mean = groupfiles_mean';
Fillin.DATANAME = dataname;
Fillin.VALUE = dataForPLS;

for indSub = 1:numel(groupfiles)
    fprintf('%s ', Fillin.GROUPFILES{indSub});
end

for indSub = 1:numel(groupfiles_mean)
    fprintf('%s ', Fillin.GROUPFILES_Mean{indSub});
end

for indSub = 1:size(Fillin.VALUE,1)
    fprintf('%s    %d %d\n', Fillin.DATANAME{indSub}, Fillin.VALUE{indSub, 1},...
        Fillin.VALUE{indSub, 2});
end
% 
% for indSub = 1:size(Fillin.VALUE,1)
%     fprintf('%s    %d\n', Fillin.DATANAME{indSub}, Fillin.VALUE{indSub, 1});
% end

figure; scatter([Fillin.VALUE{:, 1}], [Fillin.VALUE{:, 2}])
