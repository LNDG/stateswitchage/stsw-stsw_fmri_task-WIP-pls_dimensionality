% Plot BOLD SD alpha power

load('SDBOLD_BehavPLS_N41_YAonly_SDDim1234_logAlphaPow1234_3mm_1000P1000B_BfMRIresult.mat')

addpath('/Volumes/Kosciessa/Tools/barwitherr/')

figure; 
subplot(1,2,1);
    meanY = nanmean(result.datamatcorrs_lst{1,1}(1:2:end,:),2);    
    errorY = nanstd(result.datamatcorrs_lst{1,1}(1:2:end,:),[],2);%/sqrt(size(result.datamatcorrs_lst{1,1}(1:2:end,:),2)); 
    barwitherr(errorY, meanY);
    ylim([-1 1]);
    title('LV correlations with alpha power');
subplot(1,2,2);
    meanY = nanmean(result.datamatcorrs_lst{1,1}(2:2:end,:),2);    
    errorY = nanstd(result.datamatcorrs_lst{1,1}(2:2:end,:),[],2);%/sqrt(size(result.datamatcorrs_lst{1,1}(2:2:end,:),2)); 
    barwitherr(errorY, meanY);    
    ylim([-1 1]);
    title('LV correlations with spatial BOLD dimensionality');

result.usc(1:41,1) % 41 subjects condition 1, 1st brain LV

result.stacked_behavdata(1:41,1)

cond = 4;
dataCond = (cond-1)*41:cond*41;

h = figure('units','normalized','position',[.1 .1 .3 .7]);
subplot(2,1,1);
    scatter(result.stacked_behavdata(dataCond,2),-1*(result.usc(dataCond,1)), 50,'filled', 'r');
    hline = lsline; ylim([100 400]);
    xlabel('Spatial BOLD dimensionality'); ylabel('BOLD SD Brainscore');
    r = corrcoef(result.stacked_behavdata(dataCond,2),-1*(result.usc(dataCond,1)));
    legend([hline], {['r = ', num2str(r(2))]}, 'location', 'NorthEast'); legend('boxoff');
    title({'Higher BOLD variance is related';' to more integrated BOLD signals'});
subplot(2,1,2);
    scatter(result.stacked_behavdata(dataCond,1),-1*(result.usc(dataCond,1)),50, 'filled');
    hline = lsline; ylim([100 400]); xlim([-7 -4.5])
    xlabel('Log(Alpha Power)'); ylabel('BOLD SD Brainscore');
    [r, p] = corrcoef(result.stacked_behavdata(dataCond,1),-1*(result.usc(dataCond,1)));
    legend([hline], {['r = ', num2str(r(2))]}, 'location', 'NorthEast'); legend('boxoff');
    title({'Higher BOLD variance is related';' to stronger occipital alpha rhythms'});
set(findall(gcf,'-property','FontSize'),'FontSize',20)

pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/D_PLS_Dim/C_figures/';
figureName = 'PLS_Dim_logAlpha_Cond1';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

