
pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/D_PLS_Dim/';
load([pn.root, 'B_data/SD_STSWD_byCond/SDBOLD_BehavPLS_N44_YAonly_TotalPow1234_3mm_1000P1000B_BfMRIresult.mat'])

N = numel(subj_name);
condData = [];
for cond = 1:4
    condData(cond,:) = result.vsc((cond-1)*N+1:cond*N,1);
end

cond = 4;
dataCond = (cond-1)*N+1:cond*N;

h = figure('units','normalized','position',[.1 .1 .3 .3]);
    scatter(result.stacked_behavdata(dataCond,1),-1*(result.usc(dataCond,1)),50, 'filled');
    hline = lsline; %ylim([100 400]); %xlim([-7 -4.5])
    xlabel('Total EEG Power'); ylabel('BOLD SD Brainscore');
    [r, p] = corrcoef(result.stacked_behavdata(dataCond,1),-1*(result.usc(dataCond,1)));
    legend([hline], {['r = ', num2str(round(r(2),2))]}, 'location', 'SouthEast'); legend('boxoff');
    title({'Higher BOLD variance is related';' to higher total EEG power'});
set(findall(gcf,'-property','FontSize'),'FontSize',20)

pn.plotFolder = [pn.root, 'C_figures/']; mkdir(pn.plotFolder)
figureName = 'behavPLS_TotalPow';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');
