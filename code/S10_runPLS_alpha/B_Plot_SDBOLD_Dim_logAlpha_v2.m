% Plot BOLD SD alpha power

%load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B_PLS/B_data/SD_STSWD_task_v1/taskPLS_STSWD_task_N96_meancent_3mm_1000P1000B_ByAge_BfMRIresult.mat')

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/D_PLS_Dim/B_data/SD_STSWD_byCond/SDBOLD_BehavPLS_N93_SDDim1234_logAlphaPow1234_3mm_1000P1000B_BfMRIresult.mat')

addpath('/Volumes/Kosciessa/Tools/barwitherr/')

N = numel(subj_name); N_YA = 41;

condData = [];
for cond = 1:4
    condData(cond,:) = result.vsc((cond-1)*N+1:cond*N,1);
end

figure; 
subplot(1,2,1);
    meanY = nanmean(condData(:,1:N_YA),2);    
    errorY = nanstd(condData(:,1:N_YA),[],2);%/sqrt(size(result.datamatcorrs_lst{1,1}(1:2:end,:),2)); 
    barwitherr(errorY, meanY);
    ylim([-1 1]);
    title('LV correlations with alpha power');
subplot(1,2,2);
    meanY = nanmean(result.vsc(:,1:2:end),2);    
    errorY = nanstd(result.vsc(:,1:2:end),[],2);%/sqrt(size(result.datamatcorrs_lst{1,1}(2:2:end,:),2)); 
    barwitherr(errorY, meanY);    
    ylim([-1 1]);
    title('LV correlations with spatial BOLD dimensionality');

result.usc(1:N_YA,1) % 41 subjects condition 1, 1st brain LV

result.stacked_behavdata(1:N_YA,1)

cond = 4;
dataCond = ((cond-1)*N_YA)+1:cond*N_YA;

h = figure('units','normalized','position',[.1 .1 .3 .7]);
subplot(2,1,1);
    scatter(result.stacked_behavdata(dataCond,2),-1*(result.usc(dataCond,1)), 50,'filled', 'r');
    hline = lsline; ylim([100 400]);
    xlabel('Spatial BOLD dimensionality'); ylabel('BOLD SD Brainscore');
    r = corrcoef(result.stacked_behavdata(dataCond,2),-1*(result.usc(dataCond,1)));
    legend([hline], {['r = ', num2str(round(r(2),2))]}, 'location', 'NorthEast'); legend('boxoff');
    title({'Higher BOLD variance is related';' to more integrated BOLD signals'});
subplot(2,1,2);
    scatter(result.stacked_behavdata(dataCond,1),-1*(result.usc(dataCond,1)),50, 'filled');
    hline = lsline; ylim([100 400]); xlim([-7 -4.5])
    xlabel('Log(Alpha Power)'); ylabel('BOLD SD Brainscore');
    [r, p] = corrcoef(result.stacked_behavdata(dataCond,1),-1*(result.usc(dataCond,1)));
    legend([hline], {['r = ', num2str(round(r(2),2))]}, 'location', 'NorthEast'); legend('boxoff');
    title({'Higher BOLD variance is related';' to stronger occipital alpha rhythms'});
set(findall(gcf,'-property','FontSize'),'FontSize',20)

pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/D_PLS_Dim/C_figures/';
figureName = 'PLS_Dim_logAlpha_Cond4';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

