BASEPATH    = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/';
addpath(genpath([BASEPATH,'analyses/B_PLS/T_tools/NIFTI_toolbox/']));

% load data

image1 = load_nii('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/D_PLS_Dim/B_data/SD_STSWD_byCond/SDBOLD_BehavPLS_N93_SDDim1234_logAlphaPow1234_3mm_1000P1000B_BfMRIdatcorr_grp1_cond1_beh1.img');
image2 = load_nii('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/D_PLS_Dim/B_data/SD_STSWD_byCond/SDBOLD_BehavPLS_N93_SDDim1234_logAlphaPow1234_3mm_1000P1000B_BfMRIdatcorr_grp1_cond2_beh1.img');
image3 = load_nii('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/D_PLS_Dim/B_data/SD_STSWD_byCond/SDBOLD_BehavPLS_N93_SDDim1234_logAlphaPow1234_3mm_1000P1000B_BfMRIdatcorr_grp1_cond3_beh1.img');
image4 = load_nii('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/D_PLS_Dim/B_data/SD_STSWD_byCond/SDBOLD_BehavPLS_N93_SDDim1234_logAlphaPow1234_3mm_1000P1000B_BfMRIdatcorr_grp1_cond4_beh1.img');

thalMask = load_nii('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/D_PLS_Dim/B_data/SD_STSWD_byCond/CoarseThalamus.nii');

% calculate regression value for each voxel

tmpThal1 = image1.img(thalMask.img==1); tmpThal1 = reshape(tmpThal1,[],1);
tmpThal2 = image2.img(thalMask.img==1); tmpThal2 = reshape(tmpThal2,[],1);
tmpThal3 = image3.img(thalMask.img==1); tmpThal3 = reshape(tmpThal3,[],1);
tmpThal4 = image4.img(thalMask.img==1); tmpThal4 = reshape(tmpThal4,[],1);

img1.thal = mean(tmpThal1);
img2.thal = mean(tmpThal2);
img3.thal = mean(tmpThal3);
img4.thal = mean(tmpThal4);

img1.thal_se = std(tmpThal1)/sqrt(numel(tmpThal1));
img2.thal_se = std(tmpThal2)/sqrt(numel(tmpThal2));
img3.thal_se = std(tmpThal3)/sqrt(numel(tmpThal3));
img4.thal_se = std(tmpThal4)/sqrt(numel(tmpThal4));

tmpNoThal1 = image1.img(thalMask.img==0); tmpNoThal1 = reshape(tmpNoThal1,[],1);
tmpNoThal2 = image2.img(thalMask.img==0); tmpNoThal2 = reshape(tmpNoThal2,[],1);
tmpNoThal3 = image3.img(thalMask.img==0); tmpNoThal3 = reshape(tmpNoThal3,[],1);
tmpNoThal4 = image4.img(thalMask.img==0); tmpNoThal4 = reshape(tmpNoThal4,[],1);

img1.nothal = mean(tmpNoThal1);
img2.nothal = mean(tmpNoThal2);
img3.nothal = mean(tmpNoThal3);
img4.nothal = mean(tmpNoThal4);

img1.nothal_se = std(tmpNoThal1)/sqrt(numel(tmpNoThal1));
img2.nothal_se = std(tmpNoThal2)/sqrt(numel(tmpNoThal2));
img3.nothal_se = std(tmpNoThal3)/sqrt(numel(tmpNoThal3));
img4.nothal_se = std(tmpNoThal4)/sqrt(numel(tmpNoThal4));

addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/D_PLS_Dim/T_tools/raacampbell-shadedErrorBar-7002ebc/');
h = figure('units','normalized','position',[.1 .1 .7 .5]);
subplot(1,2,1); hold on;
shadedErrorBar([],[img1.thal, img2.thal, img3.thal, img4.thal],[img1.thal_se, img2.thal_se, img3.thal_se, img4.thal_se], 'lineprops','k')
h1 = plot([img1.thal, img2.thal, img3.thal, img4.thal], 'k', 'LineWidth', 2);
shadedErrorBar([],[img1.nothal, img2.nothal, img3.nothal, img4.nothal],[img1.nothal_se, img2.nothal_se, img3.nothal_se, img4.nothal_se], 'lineprops', 'r')
h2 = plot([img1.nothal, img2.nothal, img3.nothal, img4.nothal], 'r', 'LineWidth', 2);
legend([h1,h2], {'Mean of Coarse Thalamus'; 'Mean of rest of brain'}, 'location', 'East')
legend('boxoff');
title('Raw correlation values')
subplot(1,2,2); hold on;
plot(zscore([img1.thal, img2.thal, img3.thal, img4.thal]), 'LineWidth', 2);
plot(zscore([img1.nothal, img2.nothal, img3.nothal, img4.nothal]), 'LineWidth', 2);
title('Z-scored correlation values')
suptitle('Dim-related correlation increase may be global')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/D_PLS_Dim/C_figures/';
figureName = 'S10C_plotAlphaCorrsByDim_ThalNoThal';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

% save betas for each voxel