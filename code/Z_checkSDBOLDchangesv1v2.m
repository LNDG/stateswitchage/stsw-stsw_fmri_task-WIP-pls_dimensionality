IDs = {'1117';'1118';'1120';'1124';'1125';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';'1215';...
    '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';...
    '2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';'2149';'2157';...
    '2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';...
    '2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2237';'2238';...
    '2241';'2244';'2246';'2248';'2250';'2251';'2252';'2258';'2261'};

%% stability between voxels

RMat = [];
for indID = 1:numel(IDs)
    a1 = load(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B_PLS/B_data/SD_STSWD_task_v1/SD_',IDs{indID},'_BfMRIsessiondata.mat']);
    a2 = load(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B_PLS/B_data/SD_STSWD_task_v1/SD_',IDs{indID},'_v2_BfMRIsessiondata.mat']);
    RMat(indID,:,:) = corrcoef([a1.st_datamat',a2.st_datamat']);
end

figure; imagesc(squeeze(nanmean(RMat,1)));

%% stability between subjects at each voxel

for indID = 1:numel(IDs)
    a1 = load(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B_PLS/B_data/SD_STSWD_task_v1/SD_',IDs{indID},'_BfMRIsessiondata.mat']);
    a2 = load(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B_PLS/B_data/SD_STSWD_task_v1/SD_',IDs{indID},'_v2_BfMRIsessiondata.mat']);
    data1(indID,:,:) = a1.st_datamat;
    data2(indID,:,:) = a2.st_datamat;
end

RMatBetweenSubs = [];
for indVoxel = 1:size(data1,3)
    RMatBetweenSubs(indVoxel,:,:) = corrcoef([squeeze(data1(:,:,indVoxel)),squeeze(data2(:,:,indVoxel))]);
end

figure; imagesc(squeeze(nanmean(RMatBetweenSubs,1)));