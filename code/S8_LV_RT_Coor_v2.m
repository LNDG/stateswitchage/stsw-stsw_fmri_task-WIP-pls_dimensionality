%% Correlation: between-person: change in RT 4-1 -- change in dimensionality 4-1

%% load RTs (MRI)

% subject x attribute x dimensionality

load(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/A_MergeIndividualData/B_data/SummaryData_N102.mat'], 'SummaryData', 'IDs_all');

%% dimensionality Brain SD LV change

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/D_PLS_Dim/B_data/SD_STSWD_byCond/taskPLS_STSWD_N96_behavDimByCond_3mm_1000P1000B_BfMRIresult.mat')

IDs_BS = cellfun(@(x) x(4:7), subj_name, 'UniformOutput', 0)';

% find overlap between IDs_all and IDs_BS
[x, i_IDs_all, i_IDs_BS] = intersect(IDs_all, IDs_BS);

%% plot by age group

RTsbyCond = squeeze(nanmean(SummaryData.MRI.RTs_md(i_IDs_all,:,:),2));
AccbyCond = squeeze(nanmean(SummaryData.MRI.Acc_mean(i_IDs_all,:,:),2));

i_IDs_all_YA = strcmp(cellfun(@(x)x(1),IDs_all(i_IDs_all),'UniformOutput',false), '1');
i_IDs_all_OA = strcmp(cellfun(@(x)x(1),IDs_all(i_IDs_all),'UniformOutput',false), '2');

i_IDs_bs_YA = strcmp(cellfun(@(x)x(1),IDs_BS(i_IDs_BS),'UniformOutput',false), '1');
i_IDs_bs_OA = strcmp(cellfun(@(x)x(1),IDs_BS(i_IDs_BS),'UniformOutput',false), '2');

figure;
subplot(121); 
    scatter(nanmean(RTsbyCond(i_IDs_all_YA,:),2), nanmean(result.usc(i_IDs_BS(i_IDs_bs_YA),:),2), 'filled');
    %[r, p] = corrcoef(nanmean(RTsbyCond(i_IDs_all_YA,:),2), nanmean(result.usc(i_IDs_BS(i_IDs_bs_YA),:),2))
    hold on; 
    scatter(nanmean(RTsbyCond(i_IDs_all_OA,:),2), nanmean(result.usc(i_IDs_BS(i_IDs_bs_OA),:),2), 'filled');
    %[r, p] = corrcoef(nanmean(RTsbyCond(i_IDs_all_OA,:),2), nanmean(result.usc(i_IDs_BS(i_IDs_bs_OA),:),2))
    title('RTs -- SD brain score');
    xlabel('log RT'); ylabel('Brain score SD BOLD (behavioral PLS:dim)');
    [r, p] = corrcoef(nanmean(RTsbyCond(:,:),2), nanmean(result.usc(i_IDs_BS(:),:),2))
subplot(122); 
    scatter(nanmean(AccbyCond(i_IDs_all_YA,:),2), nanmean(result.usc(i_IDs_BS(i_IDs_bs_YA),:),2), 'filled');
    %[r, p] = corrcoef(nanmean(AccbyCond(i_IDs_all_YA,:),2), nanmean(result.usc(i_IDs_BS(i_IDs_bs_YA),:),2))
    hold on; scatter(nanmean(AccbyCond(i_IDs_all_OA,:),2), nanmean(result.usc(i_IDs_BS(i_IDs_bs_OA),:),2), 'filled');
    %[r, p] = corrcoef(nanmean(AccbyCond(i_IDs_all_OA,:),2), nanmean(result.usc(i_IDs_BS(i_IDs_bs_OA),:),2))
    title('Accuracy -- SD brain score');
    xlabel('Accuracy'); ylabel('Brain score SD BOLD (behavioral PLS:dim)');
    [r, p] = corrcoef(nanmean(AccbyCond(:,:),2), nanmean(result.usc(i_IDs_BS(:),:),2))

set(findall(gcf,'-property','FontSize'),'FontSize',16)


%% correlate with dimensionality

pn.dimFiles = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/D_PLS_Dim/B_data/SpatialDim/';
load([pn.dimFiles, 'N181_PCAcorr_dimensionality_spatial.mat'])

dimensionality(4,:)=[]; % remove 1124

for indID = 1:numel(IDs_BS)
    disp(['Processing ID ', IDs_BS{indID}]);
    PCAdim(indID,:) = dimensionality(indID,:);
end

figure;
subplot(121); 
    scatter(nanmean(RTsbyCond(i_IDs_all_YA,:),2), nanmean(PCAdim(i_IDs_BS(i_IDs_bs_YA),:),2), 'filled');
    %[r, p] = corrcoef(nanmean(RTsbyCond(i_IDs_all_YA,:),2), nanmean(PCAdim(i_IDs_BS(i_IDs_bs_YA),:),2))
    hold on; 
    scatter(nanmean(RTsbyCond(i_IDs_all_OA,:),2), nanmean(PCAdim(i_IDs_BS(i_IDs_bs_OA),:),2), 'filled');
    %[r, p] = corrcoef(nanmean(RTsbyCond(i_IDs_all_OA,:),2), nanmean(PCAdim(i_IDs_BS(i_IDs_bs_OA),:),2))
    title('RTs -- SD brain score');
    xlabel('log RT'); ylabel('Dimensionality');
    %[r, p] = corrcoef(nanmean(RTsbyCond(:,:),2), nanmean(PCAdim(i_IDs_BS(:),:),2))
subplot(122); 
    scatter(nanmean(AccbyCond(i_IDs_all_YA,:),2), nanmean(PCAdim(i_IDs_BS(i_IDs_bs_YA),:),2), 'filled');
    %[r, p] = corrcoef(nanmean(AccbyCond(i_IDs_all_YA,:),2), nanmean(PCAdim(i_IDs_BS(i_IDs_bs_YA),:),2))
    hold on; scatter(nanmean(AccbyCond(i_IDs_all_OA,:),2), nanmean(PCAdim(i_IDs_BS(i_IDs_bs_OA),:),2), 'filled');
    %[r, p] = corrcoef(nanmean(AccbyCond(i_IDs_all_OA,:),2), nanmean(PCAdim(i_IDs_BS(i_IDs_bs_OA),:),2))
    title('Accuracy -- SD brain score');
    xlabel('Accuracy'); ylabel('Dimensionality');
    %[r, p] = corrcoef(nanmean(AccbyCond(:,:),2), nanmean(PCAdim(i_IDs_BS(:),:),2))

set(findall(gcf,'-property','FontSize'),'FontSize',16)

% Between, but not across groups, lower PCAdim is related to lower RTs and
% higher accuracy (correlations ~.2).


% figure;
% subplot(121); 
%     scatter(RTsbyCond(i_IDs_all_YA,4)-RTsbyCond(i_IDs_all_YA,1), PCAdim(i_IDs_BS(i_IDs_bs_YA),4)-PCAdim(i_IDs_BS(i_IDs_bs_YA),1), 'filled');
%     %[r, p] = corrcoef(nanmean(RTsbyCond(i_IDs_all_YA,:),2), nanmean(PCAdim(i_IDs_BS(i_IDs_bs_YA),:),2))
%     hold on; 
%     scatter(RTsbyCond(i_IDs_all_OA,4)-RTsbyCond(i_IDs_all_OA,1), PCAdim(i_IDs_BS(i_IDs_bs_OA),4)-PCAdim(i_IDs_BS(i_IDs_bs_OA),1), 'filled');
%     %[r, p] = corrcoef(nanmean(RTsbyCond(i_IDs_all_OA,:),2), nanmean(PCAdim(i_IDs_BS(i_IDs_bs_OA),:),2))
%     title('RTs -- SD brain score');
%     xlabel('log RT'); ylabel('Dimensionality');
% subplot(122); 
%     scatter(AccbyCond(i_IDs_all_YA,4)-AccbyCond(i_IDs_all_YA,1), PCAdim(i_IDs_BS(i_IDs_bs_YA),4)-PCAdim(i_IDs_BS(i_IDs_bs_YA),1), 'filled');
%     %[r, p] = corrcoef(nanmean(AccbyCond(i_IDs_all_YA,:),2), nanmean(PCAdim(i_IDs_BS(i_IDs_bs_YA),:),2))
%     hold on; scatter(AccbyCond(i_IDs_all_OA,4)-AccbyCond(i_IDs_all_OA,1), PCAdim(i_IDs_BS(i_IDs_bs_OA),4)-PCAdim(i_IDs_BS(i_IDs_bs_OA),1), 'filled');
%     %[r, p] = corrcoef(nanmean(AccbyCond(i_IDs_all_OA,:),2), nanmean(PCAdim(i_IDs_BS(i_IDs_bs_OA),:),2))
%     title('Accuracy -- SD brain score');
%     xlabel('Accuracy'); ylabel('Dimensionality');
% 
% set(findall(gcf,'-property','FontSize'),'FontSize',16)