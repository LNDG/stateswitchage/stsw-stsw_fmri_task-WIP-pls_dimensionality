%% Plot brainscores +- CI outside PLS

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data//mri/task/analyses/D_PLS_Dim/';
pn.functions    = [pn.root, 'T_tools/']; 
addpath([pn.functions, 'barwitherr/']);
addpath([pn.functions]);

pn.data = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/D_PLS_Dim/B_data/SD_STSWD_byCond_v2/';
load([pn.data, 'meancentPLS_STSWD_SD_N97_3mm_1000P1000B_byAge_BfMRIresult.mat'])

% addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')
% cBrew = brewermap(4,'RdBu');
colorm = [230/265 25/265 25/265; 0/265 50/265 100/265];

groupsizes=result.num_subj_lst;
conditions=lv_evt_list;

conds = {'dim1'; 'dim2'; 'dim3'; 'dim4'};

condData = []; uData = [];
for indGroup = 1:2
    if indGroup == 1
        relevantEntries = 1:groupsizes(1)*numel(conds);
    elseif indGroup == 2
        relevantEntries = groupsizes(1)*numel(conds)+1:...
             groupsizes(1)*numel(conds)+groupsizes(2)*numel(conds);
    end
    for indCond = 1:4
        targetEntries = relevantEntries(conditions(relevantEntries)==indCond);        
        condData{indGroup}(indCond,:) = result.vsc(targetEntries,1);
        uData{indGroup}(indCond,:) = result.usc(targetEntries,1);
    end
end

groups = {'Young adults'; 'Old adults'};

condPairs = [1,2; 2,3; 3,4; 1,3; 2,4; 1,4];
condPairsLevel{1} = [85, 82, 84, 85, 88, 87];
condPairsLevel{2} = [53, 54, 54, 53, 54, 54];

indLV = 1;
meanCent = result.boot_result.orig_usc(:,indLV)';
ulusc_meanCent = result.boot_result.ulusc(:,indLV)';
llusc_meanCent = result.boot_result.llusc(:,indLV)';

meanY = 1-[nanmean(uData{1},2);nanmean(uData{2},2)]';

ulusc_NonMeanCent = ulusc_meanCent+(meanY-meanCent);
llusc_NonMeanCent = llusc_meanCent+(meanY-meanCent);

ulusc = ulusc_meanCent-meanCent;
llusc = llusc_meanCent-meanCent;

% figure; 
% subplot(1,2,1); hold on; bar(meanCent); plot(ulusc_meanCent); plot(llusc_meanCent)
% subplot(1,2,2); hold on; bar(meanY); plot(ulusc_NonMeanCent); plot(llusc_NonMeanCent)

errorY{1} = [llusc(1:4); ulusc(1:4)];
errorY{2} = [llusc(5:end); ulusc(5:end)];

h = figure('units','normalized','position',[.1 .1 .7 .4]);
for indGroup = 1:2
    subplot(1,2,indGroup);
    meanY = nanmean(uData{indGroup},2);    
    %errorY = nanstd(uData{indGroup},[],2)/sqrt(size(uData{indGroup},2));
    % ATTENTION: INVERTING THE SCALES FOR INTERPRETABILITY!!!
    [h1, hError] = barwitherr(errorY{indGroup}', -1*meanY);
    for indPair = 1:size(condPairs,1)
        % significance star for the difference
        [~, pval] = ttest(uData{indGroup}(condPairs(indPair,1),:), uData{indGroup}(condPairs(indPair,2), :)); % paired t-test
        % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
        % sigstars on top
        if pval <.05
            mysigstar(gca, [condPairs(indPair,1)+.1 condPairs(indPair,2)-.1], condPairsLevel{indGroup}(indPair), pval);
        end
    end
    if indGroup == 1
        set(h1(1),'FaceColor',colorm(2,:));
    else
        set(h1(1),'FaceColor',colorm(1,:));
    end
    set(h1(1),'LineWidth',2);
    set(hError(1),'LineWidth',3);
    box(gca,'off')
    set(gca, 'XTick', [1,2,3,4]);
    set(gca, 'XTickLabels', {'1'; '2'; '3'; '4'});
    xlabel('Target load')
    %xlabel('Target condition'); 
    ylabel({'Brainscore (a.u.)'})
    set(findall(gcf,'-property','FontSize'),'FontSize',30)
    if indGroup ==1
        ylim([40 90]); xlim([.25 4.75])
    else
        ylim([40 55]); xlim([.25 4.75])
    end
    title(groups{indGroup})
end

pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/D_PLS_Dim/C_figures/';
figureName = 'S15_meancentPLS_SD_YAOA_N97_nonMeanCent';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

