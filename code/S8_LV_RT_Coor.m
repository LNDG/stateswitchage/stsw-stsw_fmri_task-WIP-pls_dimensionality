%% Correlation: between-person: change in RT 4-1 -- change in dimensionality 4-1

%% load RTs (MRI)

% subject x attribute x dimensionality

load(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/A_MergeIndividualData/B_data/SummaryData_N102.mat'], 'SummaryData', 'IDs_all');

%% load brain scores

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/D_PLS_Dim/B_data/SD_Dim_ByCond_forR.mat')

% Data are stored in the following fashion:
% ya dim1, ya dim2, ya dim3, ya dim4, oa dim1, oa dim2, oa dim3, oa dim4
% The first column contains the first brain LV.

groups = {'young'; 'old'};
for indGroup = 1:2
    for indCond = 1:4
        targetEntries = strcmp(dataMat(:,2), groups{indGroup}) & cell2mat(dataMat(:,3))==indCond; 
        brainScore{indGroup,indCond} = cell2mat(dataMat(targetEntries,4));
    end
end

BrainScores_acrossGroups = [];
for indCond = 1:4
    BrainScores_acrossGroups(:,indCond) = cat(1,brainScore{:,indCond});
end

BrainScores_1min4 = BrainScores_acrossGroups(:,4)-BrainScores_acrossGroups(:,1);

figure; 
subplot(131); imagesc([brainScore{1,1}, brainScore{1,2}, brainScore{1,3}, brainScore{1,4}]);
subplot(132); imagesc([brainScore{2,1}, brainScore{2,2}, brainScore{2,3}, brainScore{2,4}]);
subplot(133); histogram(BrainScores_1min4);

IDs_BS = unique(dataMat(:,1));

%% relation between brain scores and RT differences

% find overlap between IDs_all and IDs_BS
[x, i_IDs_all, i_IDs_BS] = intersect(IDs_all, IDs_BS);

% scatterplot of brainscore (LV loading) and RT

RTsbyCond = squeeze(nanmean(SummaryData.MRI.RTs_md(i_IDs_all,:,:),2));

figure; scatter(RTsbyCond(:,4)-RTsbyCond(:,1), BrainScores_acrossGroups(i_IDs_BS,4)-BrainScores_acrossGroups(i_IDs_BS,1))
figure; scatter(RTsbyCond(:,1), BrainScores_1min4(i_IDs_BS))
figure; scatter(RTsbyCond(:,3), BrainScores_acrossGroups(i_IDs_BS,3))
figure; scatter(RTsbyCond(:,3), BrainScores_acrossGroups(i_IDs_BS,3))
figure; scatter(nanmean(RTsbyCond,2), nanmean(BrainScores_acrossGroups(i_IDs_BS,:),2))

[r, p] = corrcoef(nanmean(RTsbyCond,2), nanmean(BrainScores_acrossGroups(i_IDs_BS,:),2))


AccbyCond = squeeze(nanmean(SummaryData.MRI.Acc_mean(i_IDs_all,:,:),2));

figure; scatter(AccbyCond(:,4)-AccbyCond(:,1), BrainScores_1min4(i_IDs_BS))
figure; scatter(AccbyCond(:,1), BrainScores_1min4(i_IDs_BS))
figure; scatter(AccbyCond(:,4), BrainScores_acrossGroups(i_IDs_BS,4))
figure; scatter(nanmean(AccbyCond,2), nanmean(BrainScores_acrossGroups(i_IDs_BS,:),2))

[r, p] = corrcoef(nanmean(AccbyCond,2), nanmean(BrainScores_acrossGroups(i_IDs_BS,:),2))


%% dimensionality change

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/D_PLS_Dim/B_data/SD_STSWD_byCond/taskPLS_STSWD_N96_behavDimByCond_3mm_1000P1000B_4min1_BfMRIresult.mat')

IDs_BS = cellfun(@(x) x(4:7), subj_name, 'UniformOutput', 0)';
BrainScores_4min1 = result.vsc(:,1);

% find overlap between IDs_all and IDs_BS
[x, i_IDs_all, i_IDs_BS] = intersect(IDs_all, IDs_BS);

% scatterplot of brainscore (LV loading) and RT

RTsbyCond = squeeze(nanmean(SummaryData.MRI.RTs_md(i_IDs_all,:,:),2));

figure; scatter(RTsbyCond(:,4)-RTsbyCond(:,1), BrainScores_4min1(i_IDs_BS,:))
[r, p] = corrcoef(RTsbyCond(:,4)-RTsbyCond(:,1), BrainScores_4min1(i_IDs_BS,:))

AccbyCond = squeeze(nanmean(SummaryData.MRI.Acc_mean(i_IDs_all,:,:),2));

figure; scatter(AccbyCond(:,4)-AccbyCond(:,1), BrainScores_4min1(i_IDs_BS,:))
[r, p] = corrcoef(AccbyCond(:,4)-AccbyCond(:,1), BrainScores_4min1(i_IDs_BS,:))


%% dimensionality Brain SD LV change

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/D_PLS_Dim/B_data/SD_STSWD_byCond/taskPLS_STSWD_N96_behavDimByCond_3mm_1000P1000B_BfMRIresult.mat')

BrainScores_4min1 = result.usc(:,4)-result.usc(:,1);

% find overlap between IDs_all and IDs_BS
[x, i_IDs_all, i_IDs_BS] = intersect(IDs_all, IDs_BS);

% scatterplot of brainscore (LV loading) and RT

RTsbyCond = squeeze(nanmean(SummaryData.MRI.RTs_md(i_IDs_all,:,:),2));

figure; scatter(RTsbyCond(:,4)-RTsbyCond(:,1), BrainScores_4min1(i_IDs_BS,:))
[r, p] = corrcoef(RTsbyCond(:,4)-RTsbyCond(:,1), BrainScores_4min1(i_IDs_BS,:))

figure; scatter(nanmean(RTsbyCond,2), nanmean(result.usc(i_IDs_BS,:),2), 'filled')


AccbyCond = squeeze(nanmean(SummaryData.MRI.Acc_mean(i_IDs_all,:,:),2));

figure; scatter(AccbyCond(:,4)-AccbyCond(:,1), BrainScores_4min1(i_IDs_BS,:))
[r, p] = corrcoef(AccbyCond(:,4)-AccbyCond(:,1), BrainScores_4min1(i_IDs_BS,:))


figure; scatter(nanmean(AccbyCond,2), nanmean(RTsbyCond,2), 'filled')


figure; scatter(nanmean(AccbyCond,2), nanmean(result.usc(i_IDs_BS,:),2), 'filled')
xlabel('Accuracy across dims'); ylabel('LV loading across dims')
[r, p] = corrcoef(nanmean(AccbyCond,2), nanmean(result.usc(i_IDs_BS,:),2))


figure; scatter(nanmean(RTsbyCond(:,2:4),2)-nanmean(RTsbyCond(:,1),2), ...
    nanmean(result.usc(i_IDs_BS,2:4),2)-nanmean(result.usc(i_IDs_BS,1),2), 'filled')

%figure; scatter(nanmean(result.vsc(i_IDs_BS,:),2), nanmean(result.usc(i_IDs_BS,:),2), 'filled')
% 
% figure;
% for indAtt = 1:4
%     AccbyCond = squeeze(nanmean(SummaryData.MRI.Acc_mean(i_IDs_all,indAtt,:),2));
% 	hold on; scatter(nanmean(AccbyCond,2), nanmean(result.usc(i_IDs_BS,:),2), 'filled')
%     xlabel('Accuracy across dims'); ylabel('LV loading across dims')
%     [r, p] = corrcoef(nanmean(AccbyCond,2), nanmean(result.usc(i_IDs_BS,:),2))
% end

RTsbyCond_MRI = squeeze(nanmean(SummaryData.MRI.RTs_md(i_IDs_all,:,:),2));
RTsbyCond_EEG = squeeze(nanmean(SummaryData.EEG.RTs_md(i_IDs_all,:,:),2));

figure; scatter(nanmean(RTsbyCond_MRI(:,4)-RTsbyCond_MRI(:,1),2), nanmean(RTsbyCond_EEG(:,4)-RTsbyCond_EEG(:,1),2), 'filled')


AccbyCond_MRI = squeeze(nanmean(SummaryData.MRI.Acc_mean(i_IDs_all,:,:),2));
AccbyCond_EEG = squeeze(nanmean(SummaryData.EEG.Acc_mean(i_IDs_all,:,:),2));

figure; scatter(nanmean(AccbyCond_MRI(:,4)-AccbyCond_MRI(:,1),2), nanmean(AccbyCond_EEG(:,4)-AccbyCond_EEG(:,1),2), 'filled')

%% plot by age group

i_IDs_all_YA = strcmp(cellfun(@(x)x(1),IDs_all(i_IDs_all),'UniformOutput',false), '1');
i_IDs_all_OA = strcmp(cellfun(@(x)x(1),IDs_all(i_IDs_all),'UniformOutput',false), '2');

i_IDs_bs_YA = strcmp(cellfun(@(x)x(1),IDs_BS(i_IDs_BS),'UniformOutput',false), '1');
i_IDs_bs_OA = strcmp(cellfun(@(x)x(1),IDs_BS(i_IDs_BS),'UniformOutput',false), '2');

figure;
subplot(121); 
    scatter(nanmean(RTsbyCond(i_IDs_all_YA,:),2), nanmean(result.usc(i_IDs_BS(i_IDs_bs_YA),:),2), 'filled');
    %[r, p] = corrcoef(nanmean(RTsbyCond(i_IDs_all_YA,:),2), nanmean(result.usc(i_IDs_BS(i_IDs_bs_YA),:),2))
    hold on; 
    scatter(nanmean(RTsbyCond(i_IDs_all_OA,:),2), nanmean(result.usc(i_IDs_BS(i_IDs_bs_OA),:),2), 'filled');
    %[r, p] = corrcoef(nanmean(RTsbyCond(i_IDs_all_OA,:),2), nanmean(result.usc(i_IDs_BS(i_IDs_bs_OA),:),2))
    title('RTs -- SD brain score');
    xlabel('log RT'); ylabel('Brain score SD BOLD (behavioral PLS:dim)');
subplot(122); 
    scatter(nanmean(AccbyCond(i_IDs_all_YA,:),2), nanmean(result.usc(i_IDs_BS(i_IDs_bs_YA),:),2), 'filled');
    %[r, p] = corrcoef(nanmean(AccbyCond(i_IDs_all_YA,:),2), nanmean(result.usc(i_IDs_BS(i_IDs_bs_YA),:),2))
    hold on; scatter(nanmean(AccbyCond(i_IDs_all_OA,:),2), nanmean(result.usc(i_IDs_BS(i_IDs_bs_OA),:),2), 'filled');
    %[r, p] = corrcoef(nanmean(AccbyCond(i_IDs_all_OA,:),2), nanmean(result.usc(i_IDs_BS(i_IDs_bs_OA),:),2))
    title('Accuracy -- SD brain score');
    xlabel('Accuracy'); ylabel('Brain score SD BOLD (behavioral PLS:dim)');

set(findall(gcf,'-property','FontSize'),'FontSize',16)


%% correlate with dimensionality

pn.dimFiles = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/D_PLS_Dim/B_data/SpatialDim/';
load([pn.dimFiles, 'N181_PCAcorr_dimensionality_spatial.mat'])

dimensionality(4,:)=[]; % remove 1124

for indID = 1:numel(IDs_BS)
    disp(['Processing ID ', IDs_BS{indID}]);
    PCAdim(indID,:) = dimensionality(indID,:);
end

figure;
subplot(121); 
    scatter(nanmean(RTsbyCond(i_IDs_all_YA,:),2), nanmean(PCAdim(i_IDs_BS(i_IDs_bs_YA),:),2), 'filled');
    %[r, p] = corrcoef(nanmean(RTsbyCond(i_IDs_all_YA,:),2), nanmean(PCAdim(i_IDs_BS(i_IDs_bs_YA),:),2))
    hold on; 
    scatter(nanmean(RTsbyCond(i_IDs_all_OA,:),2), nanmean(PCAdim(i_IDs_BS(i_IDs_bs_OA),:),2), 'filled');
    %[r, p] = corrcoef(nanmean(RTsbyCond(i_IDs_all_OA,:),2), nanmean(PCAdim(i_IDs_BS(i_IDs_bs_OA),:),2))
    title('RTs -- SD brain score');
    xlabel('log RT'); ylabel('Dimensionality');
    %[r, p] = corrcoef(nanmean(RTsbyCond(:,:),2), nanmean(PCAdim(i_IDs_BS(:),:),2))
subplot(122); 
    scatter(nanmean(AccbyCond(i_IDs_all_YA,:),2), nanmean(PCAdim(i_IDs_BS(i_IDs_bs_YA),:),2), 'filled');
    %[r, p] = corrcoef(nanmean(AccbyCond(i_IDs_all_YA,:),2), nanmean(PCAdim(i_IDs_BS(i_IDs_bs_YA),:),2))
    hold on; scatter(nanmean(AccbyCond(i_IDs_all_OA,:),2), nanmean(PCAdim(i_IDs_BS(i_IDs_bs_OA),:),2), 'filled');
    %[r, p] = corrcoef(nanmean(AccbyCond(i_IDs_all_OA,:),2), nanmean(PCAdim(i_IDs_BS(i_IDs_bs_OA),:),2))
    title('Accuracy -- SD brain score');
    xlabel('Accuracy'); ylabel('Dimensionality');
    %[r, p] = corrcoef(nanmean(AccbyCond(:,:),2), nanmean(PCAdim(i_IDs_BS(:),:),2))

set(findall(gcf,'-property','FontSize'),'FontSize',16)

% Between, but not across groups, lower PCAdim is related to lower RTs and
% higher accuracy (correlations ~.2).


% figure;
% subplot(121); 
%     scatter(RTsbyCond(i_IDs_all_YA,4)-RTsbyCond(i_IDs_all_YA,1), PCAdim(i_IDs_BS(i_IDs_bs_YA),4)-PCAdim(i_IDs_BS(i_IDs_bs_YA),1), 'filled');
%     %[r, p] = corrcoef(nanmean(RTsbyCond(i_IDs_all_YA,:),2), nanmean(PCAdim(i_IDs_BS(i_IDs_bs_YA),:),2))
%     hold on; 
%     scatter(RTsbyCond(i_IDs_all_OA,4)-RTsbyCond(i_IDs_all_OA,1), PCAdim(i_IDs_BS(i_IDs_bs_OA),4)-PCAdim(i_IDs_BS(i_IDs_bs_OA),1), 'filled');
%     %[r, p] = corrcoef(nanmean(RTsbyCond(i_IDs_all_OA,:),2), nanmean(PCAdim(i_IDs_BS(i_IDs_bs_OA),:),2))
%     title('RTs -- SD brain score');
%     xlabel('log RT'); ylabel('Dimensionality');
% subplot(122); 
%     scatter(AccbyCond(i_IDs_all_YA,4)-AccbyCond(i_IDs_all_YA,1), PCAdim(i_IDs_BS(i_IDs_bs_YA),4)-PCAdim(i_IDs_BS(i_IDs_bs_YA),1), 'filled');
%     %[r, p] = corrcoef(nanmean(AccbyCond(i_IDs_all_YA,:),2), nanmean(PCAdim(i_IDs_BS(i_IDs_bs_YA),:),2))
%     hold on; scatter(AccbyCond(i_IDs_all_OA,4)-AccbyCond(i_IDs_all_OA,1), PCAdim(i_IDs_BS(i_IDs_bs_OA),4)-PCAdim(i_IDs_BS(i_IDs_bs_OA),1), 'filled');
%     %[r, p] = corrcoef(nanmean(AccbyCond(i_IDs_all_OA,:),2), nanmean(PCAdim(i_IDs_BS(i_IDs_bs_OA),:),2))
%     title('Accuracy -- SD brain score');
%     xlabel('Accuracy'); ylabel('Dimensionality');
% 
% set(findall(gcf,'-property','FontSize'),'FontSize',16)