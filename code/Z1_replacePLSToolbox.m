% Replace altered version of PLS toolbox with original version

% 16.07.18: Steffen noticed that the pls.zip that we used contained edited
% code which in some places did not make sense. It thus makes sense to
% start over from the original version (only complete modifications). This
% script will copy the 'modified' zip that was used for the original
% analyses into an archive folder and will copy over the original version.

pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/D_PLS_Dim/T_tools/';
pn.vanilla = '/Volumes/LNDG/Programs_Tools_Scripts/data_processing_repo/PLS_repo/';

%% 1. archive originally used PLS folder

mkdir([pn.root, 'Z_archive/']);
movefile([pn.root, 'pls.zip'], [pn.root, 'Z_archive/', 'pls.zip']);
movefile([pn.root, 'pls'], [pn.root, 'Z_archive/', 'pls']);

%% 2. copy over the vanilla version

copyfile([pn.vanilla, '2015-07-06_untouched/Pls'],...
    [pn.root, 'pls']);

% copy the modicfication scripts that allow to create datamats from matrices

copyfile([pn.vanilla, ...
    'PLS_toolbox_modifications/individual_modifications/batch_subject_datamat_from_matrix/plsgui/*'],...
    [pn.root, 'pls/plsgui/']);