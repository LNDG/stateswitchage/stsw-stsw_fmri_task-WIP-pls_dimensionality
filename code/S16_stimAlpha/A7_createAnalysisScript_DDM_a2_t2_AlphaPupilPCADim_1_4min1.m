% Create info file for PLS analysis

%% get behav data

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/B_data/STSWD_summary.mat')

%% YA:

clearvars -except STSWD_summary

% N = 43 YA, 1124 removed; 1125, 1213,1214,1258 (no EEG)
IDs = {'1117';'1118';'1120';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1215';...
    '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281'};

numSubs = numel(find(ismember(STSWD_summary.IDs,IDs)));
for indCond = 1:2
    for indID = 1:numel(IDs)
        disp(['Processing ID ', IDs{indID}])
        groupfiles{indID,1} = ['SD_',IDs{indID}, '_v2_BfMRIsessiondata.mat'];
        groupfiles_mean{indID,1} = ['task_',IDs{indID}, '_BfMRIsessiondata.mat'];
        curID = find(strcmp(STSWD_summary.IDs, IDs{indID}));
        if indCond == 1
            dataForPLS{(indCond-1)*numSubs+indID,1} = nanmean(STSWD_summary.HDDM_ATfixed.thresholdMRI(curID,indCond),2);
            dataForPLS{(indCond-1)*numSubs+indID,2} = nanmean(STSWD_summary.HDDM_ATfixed.nondecisionMRI(curID,indCond),2);
            dataForPLS{(indCond-1)*numSubs+indID,3} = nanmean(STSWD_summary.HDDM_ATfixed.driftMRI(curID,indCond),2);
            dataForPLS{(indCond-1)*numSubs+indID,4} = nanmean(STSWD_summary.pupil.cuePupilRelChange(curID,indCond),2);
            dataForPLS{(indCond-1)*numSubs+indID,5} = nanmean(STSWD_summary.pupil.stimPupilRelChange(curID,indCond),2);
            dataForPLS{(indCond-1)*numSubs+indID,6} = nanmean(STSWD_summary.TFR.cueAlpha(curID,indCond),2);
            dataForPLS{(indCond-1)*numSubs+indID,7} = nanmean(STSWD_summary.TFR.stimAlpha(curID,indCond),2);
            dataForPLS{(indCond-1)*numSubs+indID,8} = nanmean(STSWD_summary.PCAdim.dim(curID,indCond),2);
        elseif indCond == 2
            dataForPLS{(indCond-1)*numSubs+indID,1} = nanmean(STSWD_summary.HDDM_ATfixed.thresholdMRI(curID,4),2)-nanmean(STSWD_summary.DDM.thresholdMRI(curID,1),2);
            dataForPLS{(indCond-1)*numSubs+indID,2} = nanmean(STSWD_summary.HDDM_ATfixed.nondecisionMRI(curID,4),2)-nanmean(STSWD_summary.DDM.nondecisionMRI(curID,1),2);
            dataForPLS{(indCond-1)*numSubs+indID,3} = nanmean(STSWD_summary.HDDM_ATfixed.driftMRI(curID,4),2)-nanmean(STSWD_summary.DDM.driftMRI(curID,1),2);
            dataForPLS{(indCond-1)*numSubs+indID,4} = nanmean(STSWD_summary.pupil.cuePupilRelChange(curID,4),2)-nanmean(STSWD_summary.pupil.cuePupilRelChange(curID,1),2);
            dataForPLS{(indCond-1)*numSubs+indID,5} = nanmean(STSWD_summary.pupil.stimPupilRelChange(curID,4),2)-nanmean(STSWD_summary.pupil.stimPupilRelChange(curID,1),2);
            dataForPLS{(indCond-1)*numSubs+indID,6} = nanmean(STSWD_summary.TFR.cueAlpha(curID,4),2)-nanmean(STSWD_summary.TFR.cueAlpha(curID,1),2);
            dataForPLS{(indCond-1)*numSubs+indID,7} = nanmean(STSWD_summary.TFR.stimAlpha(curID,4),2)-nanmean(STSWD_summary.TFR.stimAlpha(curID,1),2);
            dataForPLS{(indCond-1)*numSubs+indID,8} = nanmean(STSWD_summary.PCAdim.dim(curID,4),2)-nanmean(STSWD_summary.PCAdim.dim(curID,1),2);
        end
        dataname{(indCond-1)*numSubs+indID,1} = 'behavior_data';
    end
end
groupfiles = groupfiles(find(~cellfun(@isempty,groupfiles)));
groupfiles_mean = groupfiles_mean(find(~cellfun(@isempty,groupfiles_mean)));

Fillin.GROUPFILES = groupfiles';
Fillin.GROUPFILES_Mean = groupfiles_mean';
Fillin.DATANAME = dataname;
Fillin.VALUE = dataForPLS;

% Output necessary information, has to be manually copied into .txt file
clc
for indSub = 1:numel(IDs) 
    fprintf('%s ', Fillin.GROUPFILES{indSub});
end
clc
for indSub = 1:numel(IDs) 
    fprintf('%s ', Fillin.GROUPFILES_Mean{indSub});
end
clc
for indSub = 1:size(Fillin.VALUE,1) 
    fprintf('%s    %d %d %d %d %d %d %d %d \n', ...
        Fillin.DATANAME{indSub}, Fillin.VALUE{indSub,1}, Fillin.VALUE{indSub,2}, ...
        Fillin.VALUE{indSub,3}, Fillin.VALUE{indSub,4}, ...
        Fillin.VALUE{indSub,5}, Fillin.VALUE{indSub,6}, ...
        Fillin.VALUE{indSub,7}, Fillin.VALUE{indSub,8});
end

%% OAs:

clearvars -except STSWD_summary

%N = 53 OAs
IDs = {'2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';...
    '2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';'2149';'2157';...
    '2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';...
    '2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2237';'2238';...
    '2241';'2244';'2246';'2248';'2250';'2251';'2252';'2258';'2261'};

numSubs = numel(find(ismember(STSWD_summary.IDs,IDs)));
for indCond = 1:2
    for indID = 1:numel(IDs)
        disp(['Processing ID ', IDs{indID}])
        groupfiles{indID,1} = ['SD_',IDs{indID}, '_v2_BfMRIsessiondata.mat'];
        groupfiles_mean{indID,1} = ['task_',IDs{indID}, '_BfMRIsessiondata.mat'];
        curID = find(strcmp(STSWD_summary.IDs, IDs{indID}));
        if indCond == 1
            dataForPLS{(indCond-1)*numSubs+indID,1} = nanmean(STSWD_summary.HDDM_ATfixed.thresholdMRI(curID,indCond),2);
            dataForPLS{(indCond-1)*numSubs+indID,2} = nanmean(STSWD_summary.HDDM_ATfixed.nondecisionMRI(curID,indCond),2);
            dataForPLS{(indCond-1)*numSubs+indID,3} = nanmean(STSWD_summary.HDDM_ATfixed.driftMRI(curID,indCond),2);
            dataForPLS{(indCond-1)*numSubs+indID,4} = nanmean(STSWD_summary.pupil.cuePupilRelChange(curID,indCond),2);
            dataForPLS{(indCond-1)*numSubs+indID,5} = nanmean(STSWD_summary.pupil.stimPupilRelChange(curID,indCond),2);
            dataForPLS{(indCond-1)*numSubs+indID,6} = nanmean(STSWD_summary.TFR.cueAlpha(curID,indCond),2);
            dataForPLS{(indCond-1)*numSubs+indID,7} = nanmean(STSWD_summary.TFR.stimAlpha(curID,indCond),2);
            dataForPLS{(indCond-1)*numSubs+indID,8} = nanmean(STSWD_summary.PCAdim.dim(curID,indCond),2);
        elseif indCond == 2
            dataForPLS{(indCond-1)*numSubs+indID,1} = nanmean(STSWD_summary.HDDM_ATfixed.thresholdMRI(curID,4),2)-nanmean(STSWD_summary.DDM.thresholdMRI(curID,1),2);
            dataForPLS{(indCond-1)*numSubs+indID,2} = nanmean(STSWD_summary.HDDM_ATfixed.nondecisionMRI(curID,4),2)-nanmean(STSWD_summary.DDM.nondecisionMRI(curID,1),2);
            dataForPLS{(indCond-1)*numSubs+indID,3} = nanmean(STSWD_summary.HDDM_ATfixed.driftMRI(curID,4),2)-nanmean(STSWD_summary.DDM.driftMRI(curID,1),2);
            dataForPLS{(indCond-1)*numSubs+indID,4} = nanmean(STSWD_summary.pupil.cuePupilRelChange(curID,4),2)-nanmean(STSWD_summary.pupil.cuePupilRelChange(curID,1),2);
            dataForPLS{(indCond-1)*numSubs+indID,5} = nanmean(STSWD_summary.pupil.stimPupilRelChange(curID,4),2)-nanmean(STSWD_summary.pupil.stimPupilRelChange(curID,1),2);
            dataForPLS{(indCond-1)*numSubs+indID,6} = nanmean(STSWD_summary.TFR.cueAlpha(curID,4),2)-nanmean(STSWD_summary.TFR.cueAlpha(curID,1),2);
            dataForPLS{(indCond-1)*numSubs+indID,7} = nanmean(STSWD_summary.TFR.stimAlpha(curID,4),2)-nanmean(STSWD_summary.TFR.stimAlpha(curID,1),2);
            dataForPLS{(indCond-1)*numSubs+indID,8} = nanmean(STSWD_summary.PCAdim.dim(curID,4),2)-nanmean(STSWD_summary.PCAdim.dim(curID,1),2);
        end
        dataname{(indCond-1)*numSubs+indID,1} = 'behavior_data';
    end
end
groupfiles = groupfiles(find(~cellfun(@isempty,groupfiles)));
groupfiles_mean = groupfiles_mean(find(~cellfun(@isempty,groupfiles_mean)));

Fillin.GROUPFILES = groupfiles';
Fillin.GROUPFILES_Mean = groupfiles_mean';
Fillin.DATANAME = dataname;
Fillin.VALUE = dataForPLS;

% Output necessary information, has to be manually copied into .txt file

clc
for indSub = 1:numel(IDs) 
    fprintf('%s ', Fillin.GROUPFILES{indSub});
end
clc
for indSub = 1:numel(IDs) 
    fprintf('%s ', Fillin.GROUPFILES_Mean{indSub});
end
clc
for indSub = 1:size(Fillin.VALUE,1) 
    fprintf('%s    %d %d %d %d %d %d %d %d \n', ...
        Fillin.DATANAME{indSub}, Fillin.VALUE{indSub,1}, Fillin.VALUE{indSub,2}, ...
        Fillin.VALUE{indSub,3}, Fillin.VALUE{indSub,4}, ...
        Fillin.VALUE{indSub,5}, Fillin.VALUE{indSub,6}, ...
        Fillin.VALUE{indSub,7}, Fillin.VALUE{indSub,8});
end