% Compare the variance explained by components with the component count ('dimensionality')

pn.root     = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/';
pn.data     = [pn.root, 'analyses/D_PLS_Dim/B_data/SpatialDim/'];

% N = 44 YA + 53 OA;
IDs = {'1117';'1118';'1120';'1124';'1125';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';'1215';...
    '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';...
    '2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';'2149';'2157';...
    '2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';...
    '2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2237';'2238';...
    '2241';'2244';'2246';'2248';'2250';'2251';'2252';'2258';'2261'};

dimensionality = []; explainedVar = []; explainedCumul = [];
for indID = 1:numel(IDs)
    disp(['Processing subject ', IDs{indID}]);
    load([pn.data, IDs{indID}, 'PCAcorr_dim_spatial.mat']);
    for indDim = 1:4
        dimensionality(indID,indDim) = Dimensions(indDim);
        explainedVar(indID,indDim,:) = EXPLAINED{indDim}(1:100);
        explainedCumul(indID,indDim,:) = cumsum(EXPLAINED{indDim}(1:100));
    end
    clear Dimensions EXPLAINED scores
end

figure; 
subplot(1,3,1); imagesc(squeeze(dimensionality))
subplot(1,3,2); imagesc(squeeze(explainedVar(:,1,:)))
subplot(1,3,3); imagesc(squeeze(explainedCumul(:,1,:)))


figure
subplot(1,3,2); plot(squeeze(explainedVar(:,1,:))')
subplot(1,3,3); plot(squeeze(explainedCumul(:,1,:))')

%% calculate inter-individual correlations

for indDim = 1:4
    % correlation between dimensionality and explained variance of comps
    for indComp = 1:100
        tmp_r = corrcoef(dimensionality(:,indDim), explainedVar(:,indDim,indComp));
        RMat_CompVar(indDim, indComp) = tmp_r(2);
    end
    % correlation between dimensionality and explained cumul. variance
    for indComp = 1:100
        tmp_r = corrcoef(dimensionality(:,indDim), explainedCumul(:,indDim,indComp));
        RMat_CumulVar(indDim, indComp) = tmp_r(2);
    end
end

% higher dimensionality means that less variance is explained by few early
% components

h = figure('units','normalized','position',[.1 .1 .4 .7]);
subplot(2,1,1); 
    imagesc(RMat_CompVar, [-1 1]); colorbar; 
    title('Correlation Dimensionality-Component variance')
    xlabel('PCA Component #'); ylabel('Target Load');
subplot(2,1,2);
    imagesc(RMat_CumulVar, [-1 1]); colorbar; 
    title('Correlation Dimensionality-Cumulative variance')
    xlabel('Cumulative PCA Component #'); ylabel('Target Load');
set(findall(gcf,'-property','FontSize'),'FontSize',18)

pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/D_PLS_Dim/C_figures/';
figureName = 'Dimensionality_PCAVariance_Association';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');
