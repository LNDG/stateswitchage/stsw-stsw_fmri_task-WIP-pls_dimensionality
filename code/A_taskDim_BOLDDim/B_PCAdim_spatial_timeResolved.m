function B_PCAdim_spatial_timeResolved()

% Estimate dimensionality of 90% of total spatial PCA variance

% The logic in this script is to use identical steps to those used to
% produce the SD BOLD matrices for PLS, without the final step of
% estimating SD and instead estimating spatial/temporal dimensionality.

% This also means that the actual values will be read out from the BOLD
% images located in the B_PLS folder. Make sure that this folder is
% available or copy the date directories over (I avoid this here to 
% reduce data redundancy.)

% 180306 | adapted from NKI scripts by JQK

pn.root     = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/';
pn.tools	= [pn.root, 'analyses/D_PLS_Dim/T_tools/'];  addpath(genpath(pn.tools));
pn.data     = [pn.root, 'analyses/D_PLS_Dim/B_data/'];
pn.matIn    = [pn.root, 'analyses/B_PLS/B_data/SD_STSWD_task_v1/'];
pn.savePath = [pn.data, 'SpatialDim/']; mkdir(pn.savePath);
pn.coords   = [pn.root, 'analyses/B_PLS/B_data/VoxelOverlap/'];

% N = 44 YA + 53 OA;
IDs = {'1117';'1118';'1120';'1124';'1125';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';'1215';...
    '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';...
    '2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';'2149';'2157';...
    '2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';...
    '2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2237';'2238';...
    '2241';'2244';'2246';'2248';'2250';'2251';'2252';'2258';'2261'};

for indID = 1:numel(IDs)
    disp(['Processing subject ', IDs{indID}, '.']);
    for indRun = 1:4
        % load common coordinates
        load([pn.coords, 'coords.mat'], 'final_coords_withoutZero');
        % If using preprocessing tools
        fname = ['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/B_PLS/B_data/BOLDin/',IDs{indID},'_run-',num2str(indRun),'_feat_detrended_bandpassed_FIX_MNI3mm.nii'];
        [ img ] = double(S_load_nii_2d( fname ));
        img = img(final_coords_withoutZero,:); %restrict to final_coords_withoutZero
        count = 1;
        stepsize = 30;
        EXPLAINED = [];
        for indTime = 1:stepsize:size(img,2)-stepsize
            %% Dimensions
            tic;[~, ~, ~, ~, EXPLAINED(:,count)] = pca(zscore(img(:,indTime:indTime+stepsize-1))', 'VariableWeights','variance', 'Centered', true); toc;   % spatial PCA using correlation matrix
            count = count + 1;
            % Rows of X correspond to observations and columns correspond to variables
        end
        ExplainedByRun{indID, indRun} = EXPLAINED;
    end
end

indID = 2;
figure; 
subplot(4,1,1); imagesc(ExplainedByRun{indID, 1}(1:4,:))
subplot(4,1,2); imagesc(ExplainedByRun{indID, 2}(1:4,:))
subplot(4,1,3); imagesc(ExplainedByRun{indID, 3}(1:4,:))
subplot(4,1,4); imagesc(ExplainedByRun{indID, 4}(1:4,:))

CatExplained = [];
for indID = 1:56
    for indRun = 1:4
        CatExplained = cat(1, CatExplained, ExplainedByRun{indID, indRun}(1,:));
    end
end

figure; imagesc(CatExplained)

%% play with GSR

% optional: inspect standardized dynamics
    [sortVal, sortInd] = sort(nanstd(img,[],2), 'ascend');
    h = figure; subplot(1,7,1:3);
    imagesc(zscore(img(sortInd,:),[],2)); xlabel('Volumes'); ylabel('Voxel (sorted by power)');
    set(gca,'Ydir','Normal');
    title('Z-scored BOLD dynamics')
    % global signal regression
    globalSignal = nanmean(img,1);
    tic
    residuals = [];
    for indVoxel = 1:size(img,1)
        [~, ~, residuals(:,indVoxel)] = regress(img(indVoxel,:)', (globalSignal-nanmean(globalSignal))'); % No demeaning is done here. Grave error. -JQK
    end
    toc
    residuals = residuals';
    subplot(1,7,4:6);
    imagesc(zscore(residuals(sortInd,:),[],2)); xlabel('Volumes'); ylabel('Voxel (sorted by power)');
    set(gca,'Ydir','Normal');
    title('Z-scored BOLD dynamics (following GSR)')
    % power
    subplot(1,7,7); plot(nanstd(img(sortInd,:),[],2)); xlim([0 size(img,1)])
    view([90 -90]); set(gca,'Ydir','Normal');
    title('Power')
    set(findall(gcf,'-property','FontSize'),'FontSize',18);
    
    
    count = 1;
    stepsize = 30;
    EXPLAINED = [];
    for indTime = 1:stepsize:size(img,2)-stepsize
        %% Dimensions
        tic;[~, ~, ~, ~, EXPLAINED(:,count)] = pca(residuals(:,indTime:indTime+stepsize-1)', 'VariableWeights','variance', 'Centered', true); toc;   % spatial PCA using correlation matrix
        count = count + 1;
        % Rows of X correspond to observations and columns correspond to variables
    end

    figure; imagesc(EXPLAINED)
    figure; imagesc(ExplainedByRun{2, 4})