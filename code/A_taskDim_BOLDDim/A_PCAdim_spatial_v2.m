function A_PCAdim_spatial_v2()

% Estimate dimensionality of 90% of total spatial PCA variance

% The logic in this script is to use identical steps to those used to
% produce the SD BOLD matrices for PLS, without the final step of
% estimating SD and instead estimating spatial/temporal dimensionality.

% This also means that the actual values will be read out from the BOLD
% images located in the B_PLS folder. Make sure that this folder is
% available or copy the date directories over (I avoid this here to 
% reduce data redundancy.)

% 180306 | adapted from NKI scripts by JQK

pn.root     = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/';
pn.tools	= [pn.root, 'analyses/D_PLS_Dim/T_tools/'];  addpath(genpath(pn.tools));
pn.data     = [pn.root, 'analyses/D_PLS_Dim/B_data/'];
pn.BOLDin   = [pn.root, 'analyses/B3_PLS_mean_v3/B_data/BOLDin/'];
pn.matIn    = [pn.root, 'analyses/B_PLS/B_data/SD_STSWD_task_v1/'];
pn.savePath = [pn.data, 'SpatialDim/']; mkdir(pn.savePath);
pn.coords   = [pn.root, 'analyses/B3_PLS_mean_v3/B_data/VoxelOverlap/'];

% N = 44 YA + 53 OA;
IDs = {'1117';'1118';'1120';'1124';'1125';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';'1215';...
    '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';...
    '2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';'2149';'2157';...
    '2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';...
    '2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2237';'2238';...
    '2241';'2244';'2246';'2248';'2250';'2251';'2252';'2258';'2261'};

numConds_raw = 4; % number of conditions may have changed in PLS input structure during preproc

for indID = 1:numel(IDs)
    
    disp(['Processing subject ', IDs{indID}, '.']);

    %% create the SDBOLD datamats

    % load subject's sessiondata file
    a = load([pn.matIn, 'task_', IDs{indID}, '_BfMRIsessiondata.mat']);

    % load common coordinates
    load([pn.coords, 'coords_N97.mat'], 'final_coords_withoutZero');

    conditions=a.session_info.condition(1:numConds_raw);
    a = rmfield(a,'st_datamat');
    a = rmfield(a,'st_coords');

    %replace fields with correct info.
    a.session_info.datamat_prefix   = (['SD_', IDs{indID}, '_BfMRIsessiondata']); % SD PLS file name
    a.st_coords = final_coords_withoutZero; % constrain analysis to shared non-zero GM voxels
    a.pls_data_path = pn.matIn;

    % initialize this subject's datamat
    a.st_datamat = zeros(numel(conditions),numel(final_coords_withoutZero)); %(indCond voxel)

    % intialize indCond specific scan count for populating cond_data
    clear count cond_data block_scan;
    for indCond = 1:numel(conditions)
        count{indCond} = 0;
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % within each block express each scan as deviation from indBlock's
    % temporal mean.Concatenate all these deviation values into one
    % long condition specific set of scans that were normalized for
    % block-to-block differences in signal strength. 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    % for each condition identify its scans within  runs
    % and prepare where to put indCond specific normalized data
    for indCond = 1:numel(conditions)
        tot_num_scans = 0;
        for run = 1:a.session_info.num_runs
            if a.session_info.run(run).num_scans==0
                disp(['No data for ' IDs{indID} ' in ', num2str(run)]);
                continue;
            end
            onsets = a.session_info.run(run).blk_onsets{indCond}+1; % +1 is because we need matlab indexing convention (damain)
            lengths = a.session_info.run(run).blk_length{indCond};
            for indBlock = 1:numel(onsets)
                block_scans{indCond}{run}{indBlock} = onsets(indBlock)-1+[1:lengths(indBlock)];
                this_length = lengths(indBlock);
                if max(block_scans{indCond}{run}{indBlock}>a.session_info.run(run).num_scans)
                    disp(['Problem: ' IDs{indID} ' something wrong in ', num2str(run)]);
                    block_scans{indCond}{run}{indBlock} = intersect(block_scans{indCond}{run}{indBlock},[1:a.session_info.run(run).num_scans]);
                    this_length = numel(block_scans{indCond}{run}{indBlock});
                end
                tot_num_scans = tot_num_scans + this_length;
            end
        end
        % create empty matrix with dimensions coords (rows) by total # of scans (columns).
        cond_data{indCond} = zeros(numel(final_coords_withoutZero),tot_num_scans);
    end

    %% Read data from nifty

    for run = 1:a.session_info.num_runs

        %% Load NIfTI
        % load nifti file for this run; %(x by y by z by time)
        % check for nii or nii.gz, unzip .gz and reshape to 2D

        fname = [pn.BOLDin IDs{indID} '_run-' num2str(run) '_feat_detrended_bandpassed_FIX_MNI3mm_std.nii'];
        
        if ~exist(fname) || isempty(a.session_info.run(run).data_files)
            warning(['File not available: ', IDs{indID}, ' Run ', num2str(run)]);
            continue;
        end
        
        % If using preprocessing tools
        [ img ] = double(S_load_nii_2d( fname ));
        img = img(final_coords_withoutZero,:); %restrict to final_coords_withoutZero

        %% Now, proceed with creating SD datamat...

        for indCond = 1:numel(conditions)
            for indBlock = 1:numel(block_scans{indCond}{run})
                block_data = img(:,block_scans{indCond}{run}{indBlock});% (vox time)
                % normalize block_data to global indBlock mean = 100.
                block_data = 100*block_data/mean(mean(block_data));
                % temporal mean of this indBlock
                block_mean = mean(block_data,2); % (vox) - this should be 100
                % express scans in this indBlock as deviations from block_mean
                % and append to cond_data
                good_vox = find(block_mean);
                for t = 1:size(block_data,2)
                    count{indCond} = count{indCond} + 1;
                    cond_data{indCond}(good_vox,count{indCond}) = block_data(good_vox,t) - block_mean(good_vox);%must decide about perc change option here!!??
                end % time point
            end % block
        end % condition
        
    end % run
    
    %% Calculate dimensionality for each condition across runs
    
    for indCond = 1:numel(conditions)

        %% Dimensions
        tic;[coeff.RAW{indCond}, scores{indCond}, ~, ~, EXPLAINED{indCond}] = pca(cond_data{indCond}', 'VariableWeights','variance', 'Centered', true); toc;   % spatial PCA using correlation matrix
        % Rows of X correspond to observations and columns correspond to variables

        %% Extracting components explaining min. 90% variance
        criterion = 90;

        cumulExplained = cumsum(EXPLAINED{indCond});
        Dimensions(1, indCond) = find(round(cumulExplained)>=criterion,1,'first');

        %% standardize coeff scores to gain a comparable correlation matrix
        % to standardize, the scores (principal component scores) will be 
        % correlated with coeff.weights columnwise. I.e. each voxels TS is 
        % correlated with coeff.weights
        % for spatial PCA matrix has to be transposed 884*171922 -> 171922*884

        try
        coeff.STAND{indCond} = corr(scores{indCond}, cond_data{indCond}');
        catch ME
        disp (ME.message)
        end

        %% create coefficient/eigenvector matrix of the rotated solution
        % with the exact number of dimenions for the subject previously calculated
        try
        coeff.STAND_ROT{indCond}=rotatefactors(coeff.STAND{indCond}(:, 1:Dimensions(1, indCond)));%create rotated versions of standardized coeffs above. Default is varimax... 
        catch ME
        disp (ME.message)
        end

    end % condition
   
%% save individual .mats

save([pn.savePath, IDs{indID}, 'PCAcorr_dim_spatial_v2.mat'], 'Dimensions', 'coeff', 'EXPLAINED', 'scores'); %note that EXPLAINED here is from typical, unrotated solution. 
disp (['saved to: ', pn.savePath, IDs{indID}]);
     
end % subject loop

%% collapse across subjects

for indID=1:length(IDs)
    load ([pn.savePath, IDs{indID}, 'PCAcorr_dim_spatial_v2.mat'], 'Dimensions');
    dimensionality(indID,:)=Dimensions;
end
save([pn.savePath, 'N181_PCAcorr_dimensionality_spatial_v2.mat'], 'dimensionality', 'IDs');

end % function
