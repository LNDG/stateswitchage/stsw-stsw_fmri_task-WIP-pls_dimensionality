% Create info file for PLS analysis

% for v6: include AMF, relevant DDM parameters

clear all; clc;

%% get EEG-based attention factor & HDDM data

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S14_Gamma/B_data/Z2_EEGAttentionFactor_YAOA.mat', 'EEGAttentionFactor')
load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/D_DDM/B_data/HDDM_summary.mat', 'HDDM_summary')

%% YA:

% N = 37 YA, 1151,1252,1228,1247 excluded
IDs = {'1117';'1118';'1120';'1125';'1126';'1131';'1132';'1135';'1136';...
    '1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';'1215';...
    '1216';'1219';'1223';'1227';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1250';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281'};

% get Dim

numSubs = numel(find(ismember(EEGAttentionFactor.IDs,IDs)));
IDs = IDs(ismember(IDs,EEGAttentionFactor.IDs));

Dimensionality = []; Dimensionality_block = [];
for indID = 1:numel(IDs)
    try
        load(['/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP/B5_dimensionality/B_data/D_dimensionality/',...
            IDs{indID},'PCAcorr_dim_spatial.mat'])
        Dimensionality(indID,:,:) = Dimensions;
        Dimensionality_block(indID,:,:) = Dimensions_block;
    catch
        Dimensionality(indID,:,:) = NaN;
        Dimensionality_block(indID,:,:) = NaN;
    end
end

activeIDs = [];
for indCond = 1
    for indID = 1:numel(IDs)
        disp(['Processing ID ', IDs{indID}])
        groupfiles{indID,1} = ['SD_',IDs{indID}, '_v6_BfMRIsessiondata.mat'];
        groupfiles_mean{indID,1} = ['task_',IDs{indID}, '_BfMRIsessiondata.mat'];
        curID = find(strcmp(EEGAttentionFactor.IDs, IDs{indID}));
        dataForPLS{(indCond-1)*numSubs+indID,1} = EEGAttentionFactor.PCAalphaGamma(curID);
        % add DDM results
        curID = find(strcmp(HDDM_summary.IDs, IDs{indID}));
        activeIDs = [activeIDs, curID];
        dataForPLS{(indCond-1)*numSubs+indID,2} = HDDM_summary.nondecisionMRI(curID,1);
        X = [1 2; 1 3; 1 4];
        b = regress(HDDM_summary.nondecisionMRI(curID,2:4)', X);
        dataForPLS{(indCond-1)*numSubs+indID,3} = b(2);
        dataForPLS{(indCond-1)*numSubs+indID,4} = HDDM_summary.driftMRI(curID,1);
        X = [1 2; 1 3; 1 4];
        b = regress(HDDM_summary.driftMRI(curID,2:4)', X);
        dataForPLS{(indCond-1)*numSubs+indID,5} = b(2);
        dataForPLS{(indCond-1)*numSubs+indID,6} = HDDM_summary.thresholdMRI(curID,1);
        % add Dim results
        X = [1 2; 1 3; 1 4];
        b = regress(squeeze(Dimensionality_block(indID,16,2:4)), X);
        tmpData = b(2);
        if isnan(tmpData)
            tmpData = 0;
        end
        dataForPLS{(indCond-1)*numSubs+indID,7} = tmpData;        
        dataname{(indCond-1)*numSubs+indID,1} = 'behavior_data';
    end
end
groupfiles = groupfiles(find(~cellfun(@isempty,groupfiles)));
groupfiles_mean = groupfiles_mean(find(~cellfun(@isempty,groupfiles_mean)));

Fillin.GROUPFILES = groupfiles';
Fillin.GROUPFILES_Mean = groupfiles_mean';
Fillin.DATANAME = dataname;
Fillin.VALUE = dataForPLS;

% Output necessary information, has to be manually copied into .txt file

clc;
for indSub = 1:numel(IDs) 
    fprintf('%s ', Fillin.GROUPFILES{indSub});
end

clc;
for indSub = 1:numel(IDs) 
    fprintf('%s ', Fillin.GROUPFILES_Mean{indSub});
end

clc;
for indSub = 1:size(Fillin.VALUE,1) 
    fprintf('%s    %d %d %d %d %d %d %d \n', Fillin.DATANAME{indSub}, Fillin.VALUE{indSub,1}, ...
        Fillin.VALUE{indSub,2}, Fillin.VALUE{indSub,3}, Fillin.VALUE{indSub,4}, ...
        Fillin.VALUE{indSub,5}, Fillin.VALUE{indSub,6}, Fillin.VALUE{indSub,7});
end

%% OA:

%N = 51 OAs, 2131, 2237 excl.
IDs = {'2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';...
    '2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';'2149';'2157';...
    '2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';...
    '2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2238';...
    '2241';'2244';'2246';'2248';'2250';'2251';'2252';'2258';'2261'};

% get Dim

numSubs = numel(find(ismember(EEGAttentionFactor.IDs,IDs)));
IDs = IDs(ismember(IDs,EEGAttentionFactor.IDs));

Dimensionality = []; Dimensionality_block = [];
for indID = 1:numel(IDs)
    try
        load(['/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP/B5_dimensionality/B_data/D_dimensionality/',...
            IDs{indID},'PCAcorr_dim_spatial.mat'])
        Dimensionality(indID,:,:) = Dimensions;
        Dimensionality_block(indID,:,:) = Dimensions_block;
    catch
        Dimensionality(indID,:,:) = NaN;
        Dimensionality_block(indID,:,:) = NaN;
    end
end

for indCond = 1
    for indID = 1:numel(IDs)
        disp(['Processing ID ', IDs{indID}])
        groupfiles{indID,1} = ['SD_',IDs{indID}, '_v6_BfMRIsessiondata.mat'];
        groupfiles_mean{indID,1} = ['task_',IDs{indID}, '_BfMRIsessiondata.mat'];
        curID = find(strcmp(EEGAttentionFactor.IDs, IDs{indID}));
        dataForPLS{(indCond-1)*numSubs+indID,1} = EEGAttentionFactor.PCAalphaGamma(curID);
        % add DDM results
        curID = find(strcmp(HDDM_summary.IDs, IDs{indID}));
        dataForPLS{(indCond-1)*numSubs+indID,2} = HDDM_summary.nondecisionMRI(curID,1);
        X = [1 2; 1 3; 1 4];
        b = regress(HDDM_summary.nondecisionMRI(curID,2:4)', X);
        dataForPLS{(indCond-1)*numSubs+indID,3} = b(2);
        dataForPLS{(indCond-1)*numSubs+indID,4} = HDDM_summary.driftMRI(curID,1);
        X = [1 2; 1 3; 1 4];
        b = regress(HDDM_summary.driftMRI(curID,2:4)', X);
        dataForPLS{(indCond-1)*numSubs+indID,5} = b(2);
        dataForPLS{(indCond-1)*numSubs+indID,6} = HDDM_summary.thresholdMRI(curID,1);
        % add Dim results
        X = [1 2; 1 3; 1 4];
        b = regress(squeeze(Dimensionality_block(indID,16,2:4)), X);
        tmpData = b(2);
        if isnan(tmpData)
            tmpData = 0;
        end
        dataForPLS{(indCond-1)*numSubs+indID,7} = tmpData;        
        dataname{(indCond-1)*numSubs+indID,1} = 'behavior_data';
    end
end
groupfiles = groupfiles(find(~cellfun(@isempty,groupfiles)));
groupfiles_mean = groupfiles_mean(find(~cellfun(@isempty,groupfiles_mean)));

Fillin.GROUPFILES = groupfiles';
Fillin.GROUPFILES_Mean = groupfiles_mean';
Fillin.DATANAME = dataname;
Fillin.VALUE = dataForPLS;

% Output necessary information, has to be manually copied into .txt file

clc;
for indSub = 1:numel(IDs) 
    fprintf('%s ', Fillin.GROUPFILES{indSub});
end

clc;
for indSub = 1:numel(IDs) 
    fprintf('%s ', Fillin.GROUPFILES_Mean{indSub});
end

clc;
for indSub = 1:size(Fillin.VALUE,1) 
    fprintf('%s    %d %d %d %d %d %d %d \n', Fillin.DATANAME{indSub}, Fillin.VALUE{indSub,1}, ...
        Fillin.VALUE{indSub,2}, Fillin.VALUE{indSub,3}, Fillin.VALUE{indSub,4}, ...
        Fillin.VALUE{indSub,5}, Fillin.VALUE{indSub,6}, Fillin.VALUE{indSub,7});
end
