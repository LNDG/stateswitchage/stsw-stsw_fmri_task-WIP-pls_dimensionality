% Create info file for PLS analysis

% for v4: include AMF, relevant DDM parameters

clear all; clc;

%% get EEG-based attention factor & HDDM data

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S14_Gamma/B_data/Z2_EEGAttentionFactor_YAOA.mat', 'EEGAttentionFactor')
load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/D_DDM/B_data/HDDM_summary_YA_vt.mat', 'HDDM_summary')

%% YA:

% N = 37 YA, 1151,1252,1228,1247 excluded
IDs = {'1117';'1118';'1120';'1125';'1126';'1131';'1132';'1135';'1136';...
    '1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';'1215';...
    '1216';'1219';'1223';'1227';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1250';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281'};

% get Dim

Dimensionality = []; Dimensionality_block = [];
for indID = 1:numel(IDs)
    try
        load(['/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP/B5_dimensionality/B_data/D_dimensionality/',...
            IDs{indID},'PCAcorr_dim_spatial.mat'])
        Dimensionality(indID,:,:) = Dimensions;
        Dimensionality_block(indID,:,:) = Dimensions_block;
    catch
        Dimensionality(indID,:,:) = NaN;
        Dimensionality_block(indID,:,:) = NaN;
    end
end

numSubs = numel(find(ismember(EEGAttentionFactor.IDs,IDs)));
IDs = IDs(ismember(IDs,EEGAttentionFactor.IDs));
for indCond = 1
    for indID = 1:numel(IDs)
        disp(['Processing ID ', IDs{indID}])
        groupfiles{indID,1} = ['SD_',IDs{indID}, '_v4_BfMRIsessiondata.mat'];
        groupfiles_mean{indID,1} = ['task_',IDs{indID}, '_BfMRIsessiondata.mat'];
        curID = find(strcmp(EEGAttentionFactor.IDs, IDs{indID}));
        dataForPLS{(indCond-1)*numSubs+indID,1} = EEGAttentionFactor.PCAalphaGamma(curID);
        % add DDM results
        curID = find(strcmp(HDDM_summary.IDs, IDs{indID}));
        dataForPLS{(indCond-1)*numSubs+indID,2} = HDDM_summary.nondecisionMRI(curID,1);
        dataForPLS{(indCond-1)*numSubs+indID,3} = squeeze(nanmean(HDDM_summary.nondecisionMRI(curID,2:4)))- HDDM_summary.nondecisionMRI(curID,1);
        dataForPLS{(indCond-1)*numSubs+indID,4} = HDDM_summary.driftMRI(curID,1);
        dataForPLS{(indCond-1)*numSubs+indID,5} = squeeze(nanmean(HDDM_summary.driftMRI(curID,2:4)))- HDDM_summary.driftMRI(curID,1);
        dataForPLS{(indCond-1)*numSubs+indID,6} = HDDM_summary.thresholdMRI(curID,1);
        % add Dim results
        tmpData = squeeze(nanmean(Dimensionality_block(indID,16,2:4),3))-Dimensionality_block(indID,16,1);
        if isnan(tmpData)
            tmpData = 0;
        end
        dataForPLS{(indCond-1)*numSubs+indID,7} = tmpData;        
        dataname{(indCond-1)*numSubs+indID,1} = 'behavior_data';
    end
end
groupfiles = groupfiles(find(~cellfun(@isempty,groupfiles)));
groupfiles_mean = groupfiles_mean(find(~cellfun(@isempty,groupfiles_mean)));

Fillin.GROUPFILES = groupfiles';
Fillin.GROUPFILES_Mean = groupfiles_mean';
Fillin.DATANAME = dataname;
Fillin.VALUE = dataForPLS;

% Output necessary information, has to be manually copied into .txt file

clc;
for indSub = 1:numel(IDs) 
    fprintf('%s ', Fillin.GROUPFILES{indSub});
end

clc;
for indSub = 1:numel(IDs) 
    fprintf('%s ', Fillin.GROUPFILES_Mean{indSub});
end

clc;
for indSub = 1:size(Fillin.VALUE,1) 
    fprintf('%s    %d %d %d %d %d %d %d \n', Fillin.DATANAME{indSub}, Fillin.VALUE{indSub,1}, ...
        Fillin.VALUE{indSub,2}, Fillin.VALUE{indSub,3}, Fillin.VALUE{indSub,4}, ...
        Fillin.VALUE{indSub,5}, Fillin.VALUE{indSub,6}, Fillin.VALUE{indSub,7});
end
