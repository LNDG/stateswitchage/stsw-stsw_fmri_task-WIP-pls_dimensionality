restoredefaultpath
clear all; clc;

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/';
pn.plstoolbox   = [pn.root, 'D2_PLS_VarTbx/T_tools/PLS_LNDG2018/Pls/']; addpath(genpath(pn.plstoolbox));
pn.plsdir       = [pn.root, 'D_PLS_Dim/B_data/SD_STSWD_byCond_v4/'];

cd(pn.plsdir);

batch_plsgui('behavPLS_STSWD_N38_SDBOLD_DDM_AMF_234min1_3mm_1000P1000B_BfMRIanalysis.txt')
batch_plsgui('behavPLS_STSWD_N38_SDBOLD_DDMred_AMF_234min1_3mm_1000P1000B_BfMRIanalysis.txt')
batch_plsgui('behavPLS_STSWD_N38_SDBOLD_AMF_234min1_3mm_1000P1000B_BfMRIanalysis.txt')
batch_plsgui('behavPLS_STSWD_N38_SDBOLD_AMF_Dim_234min1_3mm_1000P1000B_BfMRIanalysis.txt')
batch_plsgui('behavPLS_STSWD_N37_SDBOLD_AMF_Dim_234min1_3mm_1000P1000B_BfMRIanalysis.txt')
batch_plsgui('behavPLS_STSWD_N37_SDBOLD_AMF_Dim1_1_3mm_1000P1000B_BfMRIanalysis.txt')

pn.plsdir       = [pn.root, 'D_PLS_Dim/B_data/SD_STSWD_byCond_v2/'];
cd(pn.plsdir);

batch_plsgui('behavPLS_STSWD_N38_SDBOLD_AMF_Dim_234min1_3mm_1000P1000B_BfMRIanalysis.txt')
batch_plsgui('behavPLS_STSWD_N37_SDBOLD_AMF_Dim1_1_3mm_1000P1000B_BfMRIanalysis.txt')

pn.plsdir       = [pn.root, 'D_PLS_Dim/B_data/SD_STSWD_byCond_v3/'];
cd(pn.plsdir);

batch_plsgui('behavPLS_STSWD_N38_SDBOLD_AMF_Dim_234min1_3mm_1000P1000B_BfMRIanalysis.txt')
batch_plsgui('behavPLS_STSWD_N37_SDBOLD_AMF_Dim_234min1_3mm_1000P1000B_BfMRIanalysis.txt')
batch_plsgui('behavPLS_STSWD_N38_SDBOLD_DDM_AMF_234min1_3mm_1000P1000B_BfMRIanalysis.txt')
batch_plsgui('behavPLS_STSWD_N37_SDBOLD_AMF_Dim_DDM_234min1_3mm_1000P1000B_BfMRIanalysis.txt')

plsgui
