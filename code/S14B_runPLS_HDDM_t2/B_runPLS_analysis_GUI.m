restoredefaultpath

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/';
pn.plsroot      = [pn.root, 'analyses/D_PLS_Dim/'];
pn.plstoolbox   = ['/Volumes/LNDG/Programs_Tools_Scripts/data_processing_repo/PLS_repo/PLS_toolbox_modifications/PLS_GraphicFix2018/Pls/']; addpath(genpath(pn.plstoolbox));

cd([pn.plsroot, 'B_data/SD_STSWD_byCond_v2/']);

plsgui
