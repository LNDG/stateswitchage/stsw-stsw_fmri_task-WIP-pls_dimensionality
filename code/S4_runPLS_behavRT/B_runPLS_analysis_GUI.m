restoredefaultpath

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/';
pn.plsroot      = [pn.root, 'analyses/D_PLS_Dim/'];
pn.plstoolbox   = [pn.plsroot, 'T_tools/pls_mat/']; addpath(genpath(pn.plstoolbox));

cd([pn.plsroot, 'B_data/SD_STSWD_byCond/']);

plsgui
