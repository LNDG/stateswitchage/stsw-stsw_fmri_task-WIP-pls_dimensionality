% investigate behavioral relevance of LV1 brainscore

%% get individual brainscores & behavior

lvNo = 1;

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/D_PLS_Dim/B_data/SD_STSWD_byCond/behavPLS_STSWD_N97_SDBOLD_DDM1234_234min1_3mm_1000P1000B_BfMRIresult.mat')

groupsizes=result.num_subj_lst;
conditions=lv_evt_list;

conds = {'dim1'; 'dim2'; 'dim3'; 'dim4'; 'dim234min1'};

condData = []; uData = [];
for indGroup = 1:2
    if indGroup == 1
        relevantEntries = 1:groupsizes(1)*numel(conds);
    elseif indGroup == 2
        relevantEntries = groupsizes(1)*numel(conds)+1:...
             groupsizes(1)*numel(conds)+groupsizes(2)*numel(conds);
    end
    for indCond = 1:numel(conds)
        targetEntries = relevantEntries(conditions(relevantEntries)==indCond);        
        condData{indGroup}(indCond,:) = result.vsc(targetEntries,lvNo);
        uData{indGroup}(indCond,:) = result.usc(targetEntries,lvNo);
        behav_a_data{indGroup}(indCond,:) = result.stacked_behavdata(targetEntries,1);
        behav_t_data{indGroup}(indCond,:) = result.stacked_behavdata(targetEntries,2);
        behav_v_data{indGroup}(indCond,:) = result.stacked_behavdata(targetEntries,3);
        if lvNo == 3 % ATTENTION: INVERT BRAINSCORES FOR LV3
            uData{indGroup} = -1.*uData{indGroup};
        end
            
    end
%     uData{indGroup} = uData{indGroup}-repmat(mean(mean(uData{indGroup})),size(uData{indGroup},1),size(uData{indGroup},2)); % subtract mean
%     condData{indGroup} = condData{indGroup}-repmat(mean(mean(condData{indGroup})),size(condData{indGroup},1),size(condData{indGroup},2)); % subtract mean
end

% figure;
% subplot(1,2,1); imagesc(uData{1})
% subplot(1,2,2); imagesc(uData{2})
% 
% figure; imagesc(corrcoef(uData{1}'))

% 
% figure;
% subplot(1,2,1);
% plot(squeeze(nanmean(uData{1},2))); 
% hold on; plot(squeeze(nanmean(uData{2},2)));
% legend({'YA'; 'OA'}); title('Brainscore')
% subplot(1,2,2);
% plot(squeeze(nanmean(condData{1},2))); 
% hold on; plot(squeeze(nanmean(condData{2},2)));
% legend({'YA'; 'OA'}); title('Behavioral score')
% 
% figure;
% scatter(squeeze(nanmean(uData{2}(2:4,:),1)), uData{2}(1,:))

%% correlate brainscore change with DDM change

cond1 = 2:4;
cond2 = 1;

figure; 
subplot(2,3,1); 
    xData = nanmean(uData{1}(cond1,:),1)-uData{1}(cond2,:);
    yData = nanmean(behav_a_data{1}(cond1,:),1)-behav_a_data{1}(cond2,:);
    scatter(xData, yData, 'filled'); l = lsline;
    xlabel('2:4-1 change in LV brainscore'); ylabel('2:4-1 change in Threshold'); title('YA: Threshold')
    [r, p] = corrcoef(xData, yData); legend([l], {['r = ', num2str(round(r(2),2)), '; p = ', num2str(round(p(2),3))]});
subplot(2,3,2); 
    xData = nanmean(uData{1}(cond1,:),1)-uData{1}(cond2,:);
    yData = nanmean(behav_t_data{1}(cond1,:),1)-behav_t_data{1}(cond2,:);
    scatter(xData, yData, 'filled'); l = lsline;  l = lsline;
    xlabel('2:4-1 change in LV brainscore'); ylabel('2:4-1 change in ND time'); title('YA: ND time')
    [r, p] = corrcoef(xData, yData); legend([l], {['r = ', num2str(round(r(2),2)), '; p = ', num2str(round(p(2),3))]});
subplot(2,3,3);
    xData = nanmean(uData{1}(cond1,:),1)-uData{1}(cond2,:);
    yData = nanmean(behav_v_data{1}(cond1,:),1)-behav_v_data{1}(cond2,:);
    scatter(xData, yData, 'filled'); l = lsline;  l = lsline;
    xlabel('2:4-1 change in LV brainscore'); ylabel('2:4-1 change in Drift rate'); title('YA: Drift rate')
    [r, p] = corrcoef(xData, yData); legend([l], {['r = ', num2str(round(r(2),2)), '; p = ', num2str(round(p(2),3))]});
subplot(2,3,4);
    xData = nanmean(uData{2}(cond1,:),1)-uData{2}(cond2,:);
    yData = nanmean(behav_a_data{2}(cond1,:),1)-behav_a_data{2}(cond2,:);
    scatter(xData, yData, 'filled'); l = lsline;  l = lsline;
    xlabel('2:4-1 change in LV brainscore'); ylabel('2:4-1 change in Threshold'); title('OA: Threshold')
    [r, p] = corrcoef(xData, yData); legend([l], {['r = ', num2str(round(r(2),2)), '; p = ', num2str(round(p(2),3))]});
subplot(2,3,5);
    xData = nanmean(uData{2}(cond1,:),1)-uData{2}(cond2,:);
    yData = nanmean(behav_t_data{2}(cond1,:),1)-behav_t_data{2}(cond2,:);
    scatter(xData, yData, 'filled'); l = lsline;  l = lsline;
    xlabel('2:4-1 change in LV brainscore'); ylabel('2:4-1 change in ND time'); title('OA: ND time')
    [r, p] = corrcoef(xData, yData); legend([l], {['r = ', num2str(round(r(2),2)), '; p = ', num2str(round(p(2),3))]});
subplot(2,3,6);
    xData = nanmean(uData{2}(cond1,:),1)-uData{2}(cond2,:);
    yData = nanmean(behav_v_data{2}(cond1,:),1)-behav_v_data{2}(cond2,:);
    scatter(xData, yData, 'filled'); l = lsline;  l = lsline;
    xlabel('2:4-1 change in LV brainscore'); ylabel('2:4-1 change in Drift rate'); title('OA: Drift rate')
    [r, p] = corrcoef(xData, yData); legend([l], {['r = ', num2str(round(r(2),2)), '; p = ', num2str(round(p(2),3))]});

    
%% plot correlation matrices of change scores

figure; 
subplot(2,3,1);
    for cond1 = 1:4
        for cond2 = 1:4
            xData = uData{1}(cond1,:)-uData{1}(cond2,:);
            yData = behav_a_data{1}(cond1,:)-behav_a_data{1}(cond2,:);
            [r, p] = corrcoef(xData, yData);
            RMat(cond1, cond2) = r(2);
            PMat(cond1, cond2) = p(2);
        end
    end
    RMat(PMat>.05) = NaN;
    imagesc(RMat, [-1 1])
    xlabel('Cond1'); ylabel('Cond1'); title('YA: Threshold Cond1 - Cond2')
subplot(2,3,2);
    for cond1 = 1:4
        for cond2 = 1:4
            xData = uData{1}(cond1,:)-uData{1}(cond2,:);
            yData = behav_t_data{1}(cond1,:)-behav_t_data{1}(cond2,:);
            [r, p] = corrcoef(xData, yData);
            RMat(cond1, cond2) = r(2);
            PMat(cond1, cond2) = p(2);
        end
    end
    RMat(PMat>.05) = NaN;
    imagesc(RMat, [-1 1])
    xlabel('Cond1'); ylabel('Cond1'); title('YA: ND time Cond1 - Cond2')
subplot(2,3,3);
    for cond1 = 1:4
        for cond2 = 1:4
            xData = uData{1}(cond1,:)-uData{1}(cond2,:);
            yData = behav_v_data{1}(cond1,:)-behav_v_data{1}(cond2,:);
            [r, p] = corrcoef(xData, yData);
            RMat(cond1, cond2) = r(2);
            PMat(cond1, cond2) = p(2);
        end
    end
    RMat(PMat>.05) = NaN;
    imagesc(RMat, [-1 1])
    xlabel('Cond1'); ylabel('Cond1'); title('YA: Drift rate Cond1 - Cond2')
subplot(2,3,4);
    for cond1 = 1:4
        for cond2 = 1:4
            xData = uData{2}(cond1,:)-uData{2}(cond2,:);
            yData = behav_a_data{2}(cond1,:)-behav_a_data{2}(cond2,:);
            [r, p] = corrcoef(xData, yData);
            RMat(cond1, cond2) = r(2);
            PMat(cond1, cond2) = p(2);
        end
    end
    RMat(PMat>.05) = NaN;
    imagesc(RMat, [-1 1])
    xlabel('Cond1'); ylabel('Cond1'); title('OA: Threshold Cond1 - Cond2')
subplot(2,3,5);
    for cond1 = 1:4
        for cond2 = 1:4
            xData = uData{2}(cond1,:)-uData{2}(cond2,:);
            yData = behav_t_data{2}(cond1,:)-behav_t_data{2}(cond2,:);
            [r, p] = corrcoef(xData, yData);
            RMat(cond1, cond2) = r(2);
            PMat(cond1, cond2) = p(2);
        end
    end
    RMat(PMat>.05) = NaN;
    imagesc(RMat, [-1 1])
    xlabel('Cond1'); ylabel('Cond1'); title('OA: ND time Cond1 - Cond2')
subplot(2,3,6);
    for cond1 = 1:4
        for cond2 = 1:4
            xData = uData{2}(cond1,:)-uData{2}(cond2,:);
            yData = behav_v_data{2}(cond1,:)-behav_v_data{2}(cond2,:);
            [r, p] = corrcoef(xData, yData);
            RMat(cond1, cond2) = r(2);
            PMat(cond1, cond2) = p(2);
        end
    end
    RMat(PMat>.05) = NaN;
    imagesc(RMat, [-1 1]) 
    xlabel('Cond1'); ylabel('Cond1'); title('OA: Drift rate Cond1 - Cond2')

    % Change from load 1 to any higher load: 
    %   higher brainscore change, lower drift rate change
    %   higher brainscore change, higher threshold change (only YA)
    %   higher brainscore change, lower ND time change
    
    % Load increase within loads 234:
    %   higher brainscore change, higher drift rate change (OA), lower drift rate change (YA)
    %   higher brainscore change, lower threshold change
    %   higher brainscore change, higher ND time change (only OA)
    
    % calculate mean 4-1 3-1 2-1 change
    % calculate mean 4-3 4-2 3-2 change
    
    %% subcondition change
    
figure; 
subplot(2,3,1);
    xData = [uData{1}(4,:)-uData{1}(1,:); uData{1}(3,:)-uData{1}(1,:); uData{1}(2,:)-uData{1}(1,:)];
    xData = nanmean(xData,1);
    yData = [behav_a_data{1}(4,:)-behav_a_data{1}(1,:); behav_a_data{1}(3,:)-behav_a_data{1}(1,:); behav_a_data{1}(2,:)-behav_a_data{1}(1,:)];
    yData = nanmean(yData,1);
    hold on;
    scatter(xData, yData, 'filled'); lsline;
    xData = [uData{1}(4,:)-uData{1}(3,:); uData{1}(4,:)-uData{1}(2,:); uData{1}(3,:)-uData{1}(2,:)];
    xData = nanmean(xData,1);
    yData = [behav_a_data{1}(4,:)-behav_a_data{1}(3,:); behav_a_data{1}(4,:)-behav_a_data{1}(2,:); behav_a_data{1}(3,:)-behav_a_data{1}(2,:)];
    yData = nanmean(yData,1);
    scatter(xData, yData, 'filled'); lsline;
    xlabel('Change in LV1 brainscore'); ylabel('Change in Threshold'); title('YA: Threshold')
subplot(2,3,2);
    xData = [uData{1}(4,:)-uData{1}(1,:); uData{1}(3,:)-uData{1}(1,:); uData{1}(2,:)-uData{1}(1,:)];
    xData = nanmean(xData,1);
    yData = [behav_t_data{1}(4,:)-behav_t_data{1}(1,:); behav_t_data{1}(3,:)-behav_t_data{1}(1,:); behav_t_data{1}(2,:)-behav_t_data{1}(1,:)];
    yData = nanmean(yData,1);
    hold on;
    scatter(xData, yData, 'filled'); lsline;
    xData = [uData{1}(4,:)-uData{1}(3,:); uData{1}(4,:)-uData{1}(2,:); uData{1}(3,:)-uData{1}(2,:)];
    xData = nanmean(xData,1);
    yData = [behav_t_data{1}(4,:)-behav_t_data{1}(3,:); behav_t_data{1}(4,:)-behav_t_data{1}(2,:); behav_t_data{1}(3,:)-behav_t_data{1}(2,:)];
    yData = nanmean(yData,1);
    scatter(xData, yData, 'filled'); lsline;
    xlabel('Change in LV1 brainscore'); ylabel('Change in ND time'); title('YA: ND time')
subplot(2,3,3);
    xData = [uData{1}(4,:)-uData{1}(1,:); uData{1}(3,:)-uData{1}(1,:); uData{1}(2,:)-uData{1}(1,:)];
    xData = nanmean(xData,1);
    yData = [behav_v_data{1}(4,:)-behav_v_data{1}(1,:); behav_v_data{1}(3,:)-behav_v_data{1}(1,:); behav_v_data{1}(2,:)-behav_v_data{1}(1,:)];
    yData = nanmean(yData,1);
    hold on;
    scatter(xData, yData, 'filled'); lsline;
    xData = [uData{1}(4,:)-uData{1}(3,:); uData{1}(4,:)-uData{1}(2,:); uData{1}(3,:)-uData{1}(2,:)];
    xData = nanmean(xData,1);
    yData = [behav_v_data{1}(4,:)-behav_v_data{1}(3,:); behav_v_data{1}(4,:)-behav_v_data{1}(2,:); behav_v_data{1}(3,:)-behav_v_data{1}(2,:)];
    yData = nanmean(yData,1);
    scatter(xData, yData, 'filled'); lsline;
    xlabel('Change in LV1 brainscore'); ylabel('Change in Drift rate'); title('YA: Drift rate')
subplot(2,3,4);
    xData = [uData{2}(4,:)-uData{2}(1,:); uData{2}(3,:)-uData{2}(1,:); uData{2}(2,:)-uData{2}(1,:)];
    xData = nanmean(xData,1);
    yData = [behav_a_data{2}(4,:)-behav_a_data{2}(1,:); behav_a_data{2}(3,:)-behav_a_data{2}(1,:); behav_a_data{2}(2,:)-behav_a_data{2}(1,:)];
    yData = nanmean(yData,1);
    hold on;
    scatter(xData, yData, 'filled'); lsline;
    xData = [uData{2}(4,:)-uData{2}(3,:); uData{2}(4,:)-uData{2}(2,:); uData{2}(3,:)-uData{2}(2,:)];
    xData = nanmean(xData,1);
    yData = [behav_a_data{2}(4,:)-behav_a_data{2}(3,:); behav_a_data{2}(4,:)-behav_a_data{2}(2,:); behav_a_data{2}(3,:)-behav_a_data{2}(2,:)];
    yData = nanmean(yData,1);
    scatter(xData, yData, 'filled'); lsline;
    xlabel('Change in LV1 brainscore'); ylabel('Change in Threshold'); title('OA: Threshold')
subplot(2,3,5);
    xData = [uData{2}(4,:)-uData{2}(1,:); uData{2}(3,:)-uData{2}(1,:); uData{2}(2,:)-uData{2}(1,:)];
    xData = nanmean(xData,1);
    yData = [behav_t_data{2}(4,:)-behav_t_data{2}(1,:); behav_t_data{2}(3,:)-behav_t_data{2}(1,:); behav_t_data{2}(2,:)-behav_t_data{2}(1,:)];
    yData = nanmean(yData,1);
    hold on;
    scatter(xData, yData, 'filled'); lsline;
    xData = [uData{2}(4,:)-uData{2}(3,:); uData{2}(4,:)-uData{2}(2,:); uData{2}(3,:)-uData{2}(2,:)];
    xData = nanmean(xData,1);
    yData = [behav_t_data{2}(4,:)-behav_t_data{2}(3,:); behav_t_data{2}(4,:)-behav_t_data{2}(2,:); behav_t_data{2}(3,:)-behav_t_data{2}(2,:)];
    yData = nanmean(yData,1);
    scatter(xData, yData, 'filled'); lsline;
    xlabel('Change in LV1 brainscore'); ylabel('Change in ND time'); title('OA: ND time')
subplot(2,3,6);
    xData = [uData{2}(4,:)-uData{2}(1,:); uData{2}(3,:)-uData{2}(1,:); uData{2}(2,:)-uData{2}(1,:)];
    xData = nanmean(xData,1);
    yData = [behav_v_data{2}(4,:)-behav_v_data{2}(1,:); behav_v_data{2}(3,:)-behav_v_data{2}(1,:); behav_v_data{2}(2,:)-behav_v_data{2}(1,:)];
    yData = nanmean(yData,1);
    hold on;
    scatter(xData, yData, 'filled'); lsline;
    xData = [uData{2}(4,:)-uData{2}(3,:); uData{2}(4,:)-uData{2}(2,:); uData{2}(3,:)-uData{2}(2,:)];
    xData = nanmean(xData,1);
    yData = [behav_v_data{2}(4,:)-behav_v_data{2}(3,:); behav_v_data{2}(4,:)-behav_v_data{2}(2,:); behav_v_data{2}(3,:)-behav_v_data{2}(2,:)];
    yData = nanmean(yData,1);
    scatter(xData, yData, 'filled'); lsline;
    xlabel('Change in LV1 brainscore'); ylabel('Change in Drift rate'); title('OA: Drift rate')

    