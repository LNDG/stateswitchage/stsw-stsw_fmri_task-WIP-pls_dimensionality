% Check how LV1 relates to behavioral measures when applying PCA

pcaOutDiff = [];

figure;

lvNo = 1;

for indRun = 1:4

    load(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/D_PLS_Dim/B_data/SD_STSWD_byCond_v5/behavPLS_STSWD_N95_SDBOLD_DDM1234_234min1_3mm_1000P1000B_v5_run',num2str(indRun),'_BfMRIresult.mat'])

    groupsizes=result.num_subj_lst;
    conditions=lv_evt_list;

    conds = {'dim1'; 'dim2'; 'dim3'; 'dim4'; 'dim234min1'};

    condData = []; uData = [];
    for indGroup = 1:2
        if indGroup == 1
            relevantEntries = 1:groupsizes(1)*numel(conds);
        elseif indGroup == 2
            relevantEntries = groupsizes(1)*numel(conds)+1:...
                 groupsizes(1)*numel(conds)+groupsizes(2)*numel(conds);
        end
        for indCond = 1:numel(conds)
            targetEntries = relevantEntries(conditions(relevantEntries)==indCond);        
            condData{indGroup}(indCond,:) = result.vsc(targetEntries,lvNo);
            uData{indGroup}(indCond,:) = result.usc(targetEntries,lvNo);
            behav_a_data{indGroup}(indCond,:) = result.stacked_behavdata(targetEntries,1);
            behav_t_data{indGroup}(indCond,:) = result.stacked_behavdata(targetEntries,2);
            behav_v_data{indGroup}(indCond,:) = result.stacked_behavdata(targetEntries,3);
            if lvNo == 3 % ATTENTION: INVERT BRAINSCORES FOR LV3
                uData{indGroup} = -1.*uData{indGroup};
            end

        end
    %     uData{indGroup} = uData{indGroup}-repmat(mean(mean(uData{indGroup})),size(uData{indGroup},1),size(uData{indGroup},2)); % subtract mean
    %     condData{indGroup} = condData{indGroup}-repmat(mean(mean(condData{indGroup})),size(condData{indGroup},1),size(condData{indGroup},2)); % subtract mean
    end

    cond1 = 4;
    cond2 = 1;

    for indGroup = 1:2

        concatData = [nanmean(behav_a_data{indGroup}(cond1,:),1);...
            nanmean(behav_t_data{indGroup}(cond1,:),1);...
            nanmean(behav_v_data{indGroup}(cond1,:),1)];
        concatData2 = [nanmean(behav_a_data{indGroup}(cond2,:),1);...
            nanmean(behav_t_data{indGroup}(cond2,:),1);...
            nanmean(behav_v_data{indGroup}(cond2,:),1)];

        pcaOutDiff{indRun,indGroup} = [];
        pcaOutDiff{indRun,indGroup} = pca(concatData-concatData2);

        subplot(4,4,(indRun-1)*4+indGroup);
            xData = nanmean(condData{indGroup}(cond1,:),1)-condData{indGroup}(cond2,:);
            yData = pcaOutDiff{indRun,indGroup}(:,1);
            scatter(xData, yData, 'filled'); l = lsline;
            xlabel('2:4-1 change in LV behav score'); ylabel('2:4-1 change in xxx');
            [r, p] = corrcoef(xData, yData); legend([l], {['r = ', num2str(round(r(2),2)), '; p = ', num2str(round(p(2),3))]});
        subplot(4,4,(indRun-1)*4+2+indGroup);
            xData = nanmean(uData{indGroup}(cond1,:),1)-uData{indGroup}(cond2,:);
            yData = pcaOutDiff{indRun,indGroup}(:,1);
            scatter(xData, yData, 'filled'); l = lsline;
            xlabel('2:4-1 change in LV brainscore'); ylabel('2:4-1 change in xxx');
            [r, p] = corrcoef(xData, yData); legend([l], {['r = ', num2str(round(r(2),2)), '; p = ', num2str(round(p(2),3))]});

    end
end

figure; scatter(pcaOutDiff{1,1}(:,1), pcaOutDiff{2,1}(:,1))

figure; scatter(pcaOutDiff{3,2}(:,1), pcaOutDiff{3,2}(:,2))