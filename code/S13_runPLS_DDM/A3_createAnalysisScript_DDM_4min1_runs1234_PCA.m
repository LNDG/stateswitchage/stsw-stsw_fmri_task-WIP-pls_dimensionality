% Create info file for PLS analysis

%% get DDM parameters from MRI session

% load data as table, convert to array
pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/D_DDM/';
dataFileMRI = [pn.root, 'B_data/MRI/MRI_QuantileOpt.csv'];
tableDataMRI = readtable(dataFileMRI, 'ReadRowNames', 1);
arrayDataMRI = table2array(tableDataMRI);
ColumnNamesMRI = tableDataMRI.Properties.VariableNames'; clear tableDataMRI;

% get individual subject data

params = {'a'; 't'; 'v'};
conditions = {'1'; '2'; '3'; '4'};

IndicesMRI = []; MeanValuesMRI = [];
for indParam = 1:numel(params)
    for indCond = 1:numel(conditions)
        strPattern=[params{indParam}, '_',conditions{indCond},'_'];
        IndicesMRI = find(contains(ColumnNamesMRI,strPattern));
        MeanValuesMRI(indParam,indCond,:) = arrayDataMRI(:,IndicesMRI);
    end
end

% get corresponding IDs

dataPath = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/D_DDM/B_data/';
csvDataMRI = csvread([dataPath,'StateSwitchDynamicTrialData_MRI.dat'],2,0);
IDsMRI = cellstr(num2str(unique(csvDataMRI(:,7))));

%% YA:

clearvars -except MeanValuesMRI IDsMRI

% N = 44 YA
IDs = {'1117';'1118';'1120';'1124';'1125';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';'1215';...
    '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281'};

% get 1st PCA component across a,t,v
concatData4 = squeeze(nanmean([MeanValuesMRI(1, 2:4, ismember(IDsMRI,IDs));...
    MeanValuesMRI(2, 2:4, ismember(IDsMRI,IDs));...
    MeanValuesMRI(3, 2:4, ismember(IDsMRI,IDs))],2));
concatData1 = squeeze([MeanValuesMRI(1, 1, ismember(IDsMRI,IDs));...
    MeanValuesMRI(2, 1, ismember(IDsMRI,IDs));...
    MeanValuesMRI(3, 1, ismember(IDsMRI,IDs))]);
pcaOutDiff = pca(concatData4-concatData1);

numSubs = numel(find(ismember(IDsMRI,IDs)));
indCond = 1;
for indID = 1:numel(IDs)
    disp(['Processing ID ', IDs{indID}])
    groupfiles{indID,1} = ['SD_',IDs{indID}, '_v5_run1_BfMRIsessiondata.mat'];
    curID = find(strcmp(IDsMRI, IDs{indID}));
    dataForPLS{(indCond-1)*numSubs+indID,1} = pcaOutDiff(indID,1);
    dataname{(indCond-1)*numSubs+indID,1} = 'behavior_data';
end
groupfiles = groupfiles(find(~cellfun(@isempty,groupfiles)));

Fillin.GROUPFILES = groupfiles';
Fillin.DATANAME = dataname;
Fillin.VALUE = dataForPLS;

% Output necessary information, has to be manually copied into .txt file

clc;
for indSub = 1:numel(IDs) 
    fprintf('%s ', Fillin.GROUPFILES{indSub});
end

clc;
for indSub = 1:size(Fillin.VALUE,1) 
    fprintf('%s    %d\n', ...
        Fillin.DATANAME{indSub}, Fillin.VALUE{indSub,1});
end

%% OAs:

clearvars -except MeanValuesMRI IDsMRI

%N = 53 OAs
IDs = {'2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';...
    '2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';'2149';'2157';...
    '2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';...
    '2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2238';...
    '2241';'2244';'2246';'2248';'2250';'2251';'2252';'2258';'2261'};

% get 1st PCA component across a,t,v
concatData4 = squeeze(nanmean([MeanValuesMRI(1, 2:4, ismember(IDsMRI,IDs));...
    MeanValuesMRI(2, 2:4, ismember(IDsMRI,IDs));...
    MeanValuesMRI(3, 2:4, ismember(IDsMRI,IDs))],2));
concatData1 = squeeze([MeanValuesMRI(1, 1, ismember(IDsMRI,IDs));...
    MeanValuesMRI(2, 1, ismember(IDsMRI,IDs));...
    MeanValuesMRI(3, 1, ismember(IDsMRI,IDs))]);
pcaOutDiff = pca(concatData4-concatData1);

numSubs = numel(find(ismember(IDsMRI,IDs)));
indCond = 1;
for indID = 1:numel(IDs)
    disp(['Processing ID ', IDs{indID}])
    groupfiles{indID,1} = ['SD_',IDs{indID}, '_v5_run1_BfMRIsessiondata.mat'];
    dataForPLS{(indCond-1)*numSubs+indID,1} = pcaOutDiff(indID,1);
    dataname{(indCond-1)*numSubs+indID,1} = 'behavior_data';
end
groupfiles = groupfiles(find(~cellfun(@isempty,groupfiles)));

Fillin.GROUPFILES = groupfiles';
Fillin.DATANAME = dataname;
Fillin.VALUE = dataForPLS;

% Output necessary information, has to be manually copied into .txt file

clc;
for indSub = 1:numel(IDs) 
    fprintf('%s ', Fillin.GROUPFILES{indSub});
end

clc;
for indSub = 1:size(Fillin.VALUE,1) 
    fprintf('%s    %d\n', ...
        Fillin.DATANAME{indSub}, Fillin.VALUE{indSub,1});
end
