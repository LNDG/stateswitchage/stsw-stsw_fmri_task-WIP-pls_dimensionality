% investigate behavioral relevance of LV1 brainscore

%% get individual brainscores & behavior

lvNo = 1;

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/D_PLS_Dim/B_data/SD_STSWD_byCond_v5/behavPLS_STSWD_N94_SDBOLD_RT_4min1_3mm_1000P1000B_v5_concatRuns_BfMRIresult.mat')

groupsizes=result.num_subj_lst;
conditions=lv_evt_list;

conds = {'run1'; 'run2'; 'run3'; 'run4'};

condData = []; uData = [];
for indGroup = 1:2
    if indGroup == 1
        relevantEntries = 1:groupsizes(1)*numel(conds);
    elseif indGroup == 2
        relevantEntries = groupsizes(1)*numel(conds)+1:...
             groupsizes(1)*numel(conds)+groupsizes(2)*numel(conds);
    end
    for indCond = 1:numel(conds)
        targetEntries = relevantEntries(conditions(relevantEntries)==indCond);        
        condData{indGroup}(indCond,:) = result.vsc(targetEntries,lvNo);
        uData{indGroup}(indCond,:) = result.usc(targetEntries,lvNo);
        behav_rt_data{indGroup}(indCond,:) = result.stacked_behavdata(targetEntries,1);    
    end
end

%% correlate brainscore change with DDM change

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/A_MergeIndividualData/B_data/SummaryData_N102_byRun.mat')

% indLoad = 1; meanAttributesByRun = [];
% for indID = 1:size(SummaryData.MRI.logRTs_md,1)
%     for indRun = 1:4
%         meanAttributesByRun(indRun,indID) = mean(find(~isnan(SummaryData.MRI.RTs_md(indID,indRun,:,indLoad))));
%     end
% end

for indID = 1:size(SummaryData.MRI.logRTs_md,1)
    y = squeeze(nanmean(nanmean(SummaryData.MRI.RTs_md(indID,:,1:4,:),2),3)); % average across attributes
    y2 = squeeze(nanmean(nanmean(SummaryData.MRI.Acc_mean(indID,:,1:4,:),2),3)); % average across attributes
    
     %x1 = (SummaryData.MRI.RTs_md(:,:,:,4)-SummaryData.MRI.RTs_md(:,:,:,1));%.*SummaryData.MRI.Acc_mean(:,:,:,1);
     %Dim4minDim1_RT_MRI(indID,:) = squeeze(nanmean(x1(indID,:,1:2),3));
%      y = squeeze(nanmean(nanmean(x1(indID,:,1:4),2),3)); % average across attributes
%      y2 = squeeze(nanmean(nanmean(x1(indID,:,1:4),2),3)); % average across attributes
    Dim4minDim1_RT_MRI(indID,:) = (y(4).*y2(4)-y(1).*y2(1));
    %Dim4minDim1_RT_MRI(indID,:) = (y(4)-y(1)).*y2(1);
    %Dim4minDim1_RT_MRI(indID,:) = (nanmean(y(4))-y(1))./y(1);
end
%Dim4minDim1_RT_MRI(isnan(Dim4minDim1_RT_MRI)) = 0;

% N = 42 YA; 1228 removed (outlier); 1234 removed (no behav data R1)
IDs = {'1117';'1118';'1120';'1124';'1125';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';'1215';...
    '1216';'1219';'1223';'1227';'1233';'1237';'1239';'1240';'1243';...
    '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281'};

idxID = find(ismember(IDs_all,IDs));

cond = 1:4;

figure;
subplot(2,2,1);
    xData = nanmean(uData{1}(cond,:),1);
    yData = squeeze(nanmean(Dim4minDim1_RT_MRI(idxID,cond),2));
    scatter(xData, yData, 'filled'); l = lsline;
    xlabel('2:4-1 change in LV brainscore'); ylabel('2:4-1 change in Threshold'); title('YA: Threshold')
    [r, p] = corrcoef(xData, yData); legend([l], {['r = ', num2str(round(r(2),2)), '; p = ', num2str(round(p(2),3))]});

subplot(2,2,3);
    xData = nanmean(condData{1}(cond,:),1);
    yData = squeeze(nanmean(Dim4minDim1_RT_MRI(idxID,cond),2));
    scatter(xData, yData, 'filled'); l = lsline;
    xlabel('2:4-1 change in LV brainscore'); ylabel('2:4-1 change in Threshold'); title('YA: Threshold')
    [r, p] = corrcoef(xData, yData); legend([l], {['r = ', num2str(round(r(2),2)), '; p = ', num2str(round(p(2),3))]});


%N = 53 OAs, 2112, 2120 removed (behav outliers)
IDs = {'2104';'2107';'2108';'2118';'2121';'2123';'2125';'2129';'2130';...
    '2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';'2149';'2157';...
    '2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';...
    '2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2238';...
    '2241';'2244';'2246';'2248';'2250';'2251';'2252';'2258';'2261'};

idxID = find(ismember(IDs_all,IDs));

cond = 1:4;

subplot(2,2,2);
    xData = nanmean(uData{2}(cond,:),1);
    yData = squeeze(nanmean(Dim4minDim1_RT_MRI(idxID,cond),2));

    scatter(xData, yData, 'filled'); l = lsline;
    xlabel('2:4-1 change in LV brainscore'); ylabel('2:4-1 change in Threshold'); title('YA: Threshold')
    [r, p] = corrcoef(xData, yData); legend([l], {['r = ', num2str(round(r(2),2)), '; p = ', num2str(round(p(2),3))]});

subplot(2,2,4);
    xData = nanmean(condData{2}(cond,:),1);
    yData = squeeze(nanmean(Dim4minDim1_RT_MRI(idxID,cond),2));

    scatter(xData, yData, 'filled'); l = lsline;
    xlabel('2:4-1 change in LV brainscore'); ylabel('2:4-1 change in Threshold'); title('YA: Threshold')
    [r, p] = corrcoef(xData, yData); legend([l], {['r = ', num2str(round(r(2),2)), '; p = ', num2str(round(p(2),3))]});
