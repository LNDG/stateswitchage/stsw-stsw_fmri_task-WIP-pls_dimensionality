% investigate behavioral relevance of LV1 brainscore

%% get individual brainscores & behavior

lvNo = 1;

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/D_PLS_Dim/B_data/SD_STSWD_byCond_v5/behavPLS_STSWD_N95_SDBOLD_DDM_PC1wDDM_234min1_3mm_1000P1000B_v5_concatRuns_BfMRIresult.mat')

groupsizes=result.num_subj_lst;
conditions=lv_evt_list;

conds = {'run1'; 'run2'; 'run3'; 'run4'};

condData = []; uData = [];
for indGroup = 1:2
    if indGroup == 1
        relevantEntries = 1:groupsizes(1)*numel(conds);
    elseif indGroup == 2
        relevantEntries = groupsizes(1)*numel(conds)+1:...
             groupsizes(1)*numel(conds)+groupsizes(2)*numel(conds);
    end
    for indCond = 1:numel(conds)
        targetEntries = relevantEntries(conditions(relevantEntries)==indCond);        
        condData{indGroup}(indCond,:) = result.vsc(targetEntries,lvNo);
        uData{indGroup}(indCond,:) = result.usc(targetEntries,lvNo);
        behav_pc1_data{indGroup}(indCond,:) = result.stacked_behavdata(targetEntries,1);
        behav_a_data{indGroup}(indCond,:) = result.stacked_behavdata(targetEntries,2);
        behav_t_data{indGroup}(indCond,:) = result.stacked_behavdata(targetEntries,3);
        behav_v_data{indGroup}(indCond,:) = result.stacked_behavdata(targetEntries,4);
        if lvNo == 3 % ATTENTION: INVERT BRAINSCORES FOR LV3
            uData{indGroup} = -1.*uData{indGroup};
        end
            
    end
%     uData{indGroup} = uData{indGroup}-repmat(mean(mean(uData{indGroup})),size(uData{indGroup},1),size(uData{indGroup},2)); % subtract mean
%     condData{indGroup} = condData{indGroup}-repmat(mean(mean(condData{indGroup})),size(condData{indGroup},1),size(condData{indGroup},2)); % subtract mean
end

% figure;
% subplot(1,2,1); imagesc(uData{1})
% subplot(1,2,2); imagesc(uData{2})
% 
% figure; imagesc(corrcoef(uData{1}'))

% 
% figure;
% subplot(1,2,1);
% plot(squeeze(nanmean(uData{1},2))); 
% hold on; plot(squeeze(nanmean(uData{2},2)));
% legend({'YA'; 'OA'}); title('Brainscore')
% subplot(1,2,2);
% plot(squeeze(nanmean(condData{1},2))); 
% hold on; plot(squeeze(nanmean(condData{2},2)));
% legend({'YA'; 'OA'}); title('Behavioral score')
% 
% figure;
% scatter(squeeze(nanmean(uData{2}(2:4,:),1)), uData{2}(1,:))

%% correlate brainscore change with DDM change

cond = 1:4;

figure; 
subplot(1,2,1);
    xData = nanmean(uData{1}(cond,:),1);
    yData = nanmean((behav_a_data{1}(cond,:)+behav_t_data{1}(cond,:)).*behav_v_data{1}(cond,:),1);
    scatter(xData, yData, 'filled'); l = lsline;
    xlabel('2:4-1 change in LV brainscore'); ylabel('2:4-1 change in Threshold'); title('YA: Threshold')
    [r, p] = corrcoef(xData, yData); legend([l], {['r = ', num2str(round(r(2),2)), '; p = ', num2str(round(p(2),3))]});
subplot(1,2,2);
    xData = nanmean(uData{2}(cond,:),1);
    yData = nanmean((+1*behav_a_data{2}(cond,:)+1*behav_t_data{2}(cond,:)).*behav_v_data{2}(cond,:),1);
    scatter(xData, yData, 'filled'); l = lsline;
    xlabel('2:4-1 change in LV brainscore'); ylabel('2:4-1 change in Threshold'); title('YA: Threshold')
    [r, p] = corrcoef(xData, yData); legend([l], {['r = ', num2str(round(r(2),2)), '; p = ', num2str(round(p(2),3))]});

figure; 
subplot(1,2,1);
    cond = 1:4;
    xData = nanmean(uData{1}(cond,:),1);
    yData = nanmean((behav_t_data{1}(cond,:)).*(behav_v_data{1}(cond,:).*behav_a_data{1}(cond,:)),1);
    scatter(xData, yData, 'filled'); l = lsline;
    xlabel('2:4-1 change in LV brainscore'); ylabel('2:4-1 change in Threshold'); title('YA: Threshold')
    [r, p] = corrcoef(xData, yData); legend([l], {['r = ', num2str(round(r(2),2)), '; p = ', num2str(round(p(2),3))]});
subplot(1,2,2);
    cond = 1:4;
    xData = nanmean(uData{2}(cond,:),1);
    yData = nanmean((behav_t_data{2}(cond,:)).*(behav_v_data{2}(cond,:).*behav_a_data{2}(cond,:)),1);
    scatter(xData, yData, 'filled'); l = lsline;
    xlabel('2:4-1 change in LV brainscore'); ylabel('2:4-1 change in Threshold'); title('YA: Threshold')
    [r, p] = corrcoef(xData, yData); legend([l], {['r = ', num2str(round(r(2),2)), '; p = ', num2str(round(p(2),3))]});

% drift(acc, positive better)-RTchange(ndt, negative better)

figure; 
subplot(2,2,1);
    cond = 1:4;
    xData = nanmean(uData{1}(cond,:),1);
    yData = nanmean((behav_v_data{1}(cond,:)-behav_t_data{1}(cond,:)).*(behav_a_data{1}(cond,:)+behav_t_data{1}(cond,:)),1);
    scatter(xData, yData, 'filled'); l = lsline;
    xlabel('2:4-1 change in LV brainscore'); ylabel('2:4-1 change in Threshold'); title('YA')
    [r, p] = corrcoef(xData, yData); legend([l], {['r = ', num2str(round(r(2),2)), '; p = ', num2str(round(p(2),3))]});
subplot(2,2,2);
    cond = 1:4;
    xData = nanmean(uData{2}(cond,:),1);
    yData = nanmean((behav_v_data{2}(cond,:)-behav_t_data{2}(cond,:)).*behav_a_data{2}(cond,:)+behav_t_data{2}(cond,:),1);
    scatter(xData, yData, 'filled'); l = lsline;
    xlabel('2:4-1 change in LV brainscore'); ylabel('2:4-1 change in Threshold'); title('OA')
    [r, p] = corrcoef(xData, yData); legend([l], {['r = ', num2str(round(r(2),2)), '; p = ', num2str(round(p(2),3))]});
subplot(2,2,3);
    cond = 1:4;
    xData = nanmean(condData{1}(cond,:),1);
    yData = nanmean((behav_v_data{1}(cond,:)-behav_t_data{1}(cond,:)).*(behav_a_data{1}(cond,:)+behav_t_data{1}(cond,:)),1);
    scatter(xData, yData, 'filled'); l = lsline;
    xlabel('2:4-1 change in LV brainscore'); ylabel('2:4-1 change in Threshold'); title('YA')
    [r, p] = corrcoef(xData, yData); legend([l], {['r = ', num2str(round(r(2),2)), '; p = ', num2str(round(p(2),3))]});
subplot(2,2,4);
    cond = 1:4;
    xData = nanmean(condData{2}(cond,:),1);
    yData = nanmean((behav_v_data{2}(cond,:)-behav_t_data{2}(cond,:)).*behav_a_data{2}(cond,:)+behav_t_data{2}(cond,:),1);
    scatter(xData, yData, 'filled'); l = lsline;
    xlabel('2:4-1 change in LV brainscore'); ylabel('2:4-1 change in Threshold'); title('OA')
    [r, p] = corrcoef(xData, yData); legend([l], {['r = ', num2str(round(r(2),2)), '; p = ', num2str(round(p(2),3))]});

figure; 
subplot(2,2,1);
    cond = 1:4;
    xData = nanmean(uData{1}(cond,:),1);
    yData = nanmean((behav_v_data{1}(cond,:)-behav_t_data{1}(cond,:)).*(behav_a_data{1}(cond,:)+behav_t_data{1}(cond,:)),1);
    scatter(xData, yData, 'filled'); l = lsline;
    xlabel('4-1 change in LV brainscore'); ylabel('4-1 change in (v-t)*a'); title('YA')
    [r, p] = corrcoef(xData, yData); legend([l], {['r = ', num2str(round(r(2),2)), '; p = ', num2str(round(p(2),3))]});
subplot(2,2,2);
    cond = 1:4;
    xData = nanmean(uData{2}(cond,:),1);
    yData = nanmean((behav_v_data{2}(cond,:)-behav_t_data{2}(cond,:)).*behav_a_data{2}(cond,:),1);
    scatter(xData, yData, 'filled'); l = lsline;
    xlabel('4-1 change in LV brainscore'); ylabel('4-1 change in (v-t)*a'); title('OA')
    [r, p] = corrcoef(xData, yData); legend([l], {['r = ', num2str(round(r(2),2)), '; p = ', num2str(round(p(2),3))]});
subplot(2,2,3);
    cond = 1:4;
    xData = nanmean(condData{1}(cond,:),1);
    yData = nanmean((behav_v_data{1}(cond,:)-behav_t_data{1}(cond,:)).*(behav_a_data{1}(cond,:)),1);
    scatter(xData, yData, 'filled'); l = lsline;
     xlabel('4-1 change in LV behavscore'); ylabel('4-1 change in (v-t)*a'); title('YA')
    [r, p] = corrcoef(xData, yData); legend([l], {['r = ', num2str(round(r(2),2)), '; p = ', num2str(round(p(2),3))]});
subplot(2,2,4);
    cond = 1:4;
    xData = nanmean(condData{2}(cond,:),1);
    yData = nanmean((behav_v_data{2}(cond,:)-behav_t_data{2}(cond,:)).*behav_a_data{2}(cond,:),1);
    scatter(xData, yData, 'filled'); l = lsline;
    xlabel('4-1 change in LV behavscore'); ylabel('4-1 change in (v-t)*a'); title('OA')
    [r, p] = corrcoef(xData, yData); legend([l], {['r = ', num2str(round(r(2),2)), '; p = ', num2str(round(p(2),3))]});
