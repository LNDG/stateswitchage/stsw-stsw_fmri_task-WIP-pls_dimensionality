%% recreate the calculated contrasts

pn.root     = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/';
pn.tools	= [pn.root, 'analyses/B_PLS/T_tools/'];  addpath(genpath(pn.tools));
pn.data     = [pn.root, 'analyses/B_PLS/B_data/'];
MATPATH     = [pn.data,'SD_STSWD_task_v1/'];
NIIPATH     = [pn.root, 'analyses/B3_PLS_mean_v3/B_data/BOLDin/'];
COORDPATH   = [pn.data, 'VoxelOverlap/'];

% N = 44 YA + 53 OA;
IDs = {'1117';'1118';'1120';'1124';'1125';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';'1215';...
    '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';...
    '2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';'2149';'2157';...
    '2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';...
    '2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2237';'2238';...
    '2241';'2244';'2246';'2248';'2250';'2251';'2252';'2258';'2261'};

numConds_raw = 4; % number of conditions may have changed in PLS input structure during preproc

for indID = 1:numel(IDs)
    disp(['Processing subject ', IDs{indID}, '.']);

%% create the SDBOLD datamats

    % load subject's sessiondata file
    a = load([MATPATH, 'task_', IDs{indID}, '_BfMRIsessiondata.mat']);

    % for each condition identify its scans within  runs
    % and prepare where to put indCond specific normalized data
    for indCond = 1:4
        tot_num_scans = 0;
        for indRun = 1:a.session_info.num_runs
            if a.session_info.run(indRun).num_scans==0
                disp(['No data for ' IDs{indID} ' in ', num2str(indRun)]);
                continue;
            end
            onsets = a.session_info.run(indRun).blk_onsets{indCond}+1; % +1 is because we need matlab indexing convention (damain)
            lengths = a.session_info.run(indRun).blk_length{indCond};
            for indBlock = 1:numel(onsets)
                block_scans{indCond}{indRun}{indBlock} = onsets(indBlock)-1+[1:lengths(indBlock)];
                this_length = lengths(indBlock);
                if max(block_scans{indCond}{indRun}{indBlock}>a.session_info.run(indRun).num_scans)
                    disp(['Problem: ' IDs{indID} ' something wrong in ', num2str(indRun)]);
                    block_scans{indCond}{indRun}{indBlock} = intersect(block_scans{indCond}{indRun}{indBlock},[1:a.session_info.run(indRun).num_scans]);
                    this_length = numel(block_scans{indCond}{indRun}{indBlock});
                end
                tot_num_scans = tot_num_scans + this_length;
            end
        end
    end

    pn.timing	= '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/analyses/A_extractDesignTiming/B_data/A_regressors/';
    
    runs = {'Run1'; 'Run2'; 'Run3'; 'Run4'};

    %% Load NIfTI file

    for indRun = 1:a.session_info.num_runs
        load([pn.timing, IDs{indID}, '_',runs{indRun},'_regressors.mat'], 'Regressors', 'RegressorInfo');
        for indCond = 1:4
            for indBlock = 1:numel(block_scans{indCond}{indRun})
                RunOnsetMat(indID, indRun,indCond,indBlock) = Regressors(block_scans{indCond}{4}{indBlock}(1),4);
                ActualRunOnsetMat(indID, indRun,indCond,indBlock) = Regressors(block_scans{indCond}{indRun}{indBlock}(1),4);
            end
        end
    end

end


% average across blocks and runs

figure; 
subplot(1,2,1); imagesc(squeeze(nanmedian(nanmedian(RunOnsetMat,4),2)))
subplot(1,2,2); imagesc(squeeze(nanmedian(nanmedian(ActualRunOnsetMat,4),2)))

% calculate average contrast 234-1 by run

figure; 
subplot(1,2,1); imagesc(squeeze(nanmedian(nanmean(RunOnsetMat(:,:,2:4,:),3)-RunOnsetMat(:,:,1,:),4)))
subplot(1,2,2); imagesc(squeeze(nanmedian(nanmedian(ActualRunOnsetMat,4),2)))

figure
subplot(1,2,1); bar(squeeze(nanmean(nanmedian(nanmean(RunOnsetMat(:,:,2:4,:),3)-RunOnsetMat(:,:,1,:),4),1)))
subplot(1,2,2); bar(squeeze(nanmean(nanmedian(nanmean(ActualRunOnsetMat(:,:,2:4,:),3)-ActualRunOnsetMat(:,:,1,:),4),1)))




%% sort by NDT change

% N = 44 YA + 53 OA;
IDs = {'1117';'1118';'1120';'1124';'1125';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';'1215';...
    '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';...
    '2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';'2149';'2157';...
    '2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';...
    '2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2237';'2238';...
    '2241';'2244';'2246';'2248';'2250';'2251';'2252';'2258';'2261'};

pn.HDDMdata = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/D_DDM/B_data/';

load([pn.HDDMdata, 'HDDM_summary.mat'], 'HDDM_summary')

idxHDDM = ismember(HDDM_summary.IDs,IDs);

[sortVal, sortIdx] = sort(nanmean(HDDM_summary.nondecisionMRI(idxHDDM,2:4),2)-HDDM_summary.nondecisionMRI(idxHDDM,1), 'ascend');

figure; 
plotData = squeeze(nanmean(nanmean(RunOnsetMat,4),2));
subplot(1,3,1); imagesc(plotData(sortIdx,:))
subplot(1,3,2); bar(nanmean(plotData(sortIdx(1:numel(sortIdx)/2),:)))
subplot(1,3,3); bar(nanmean(plotData(sortIdx(numel(sortIdx)/2+1:end),:)))

figure; 
plotData = squeeze(nanmean(nanmean(nanmean(RunOnsetMat(:,:,2:4,:),3)-RunOnsetMat(:,:,1,:),4),2));
subplot(1,3,1); imagesc(plotData(sortIdx,:))
subplot(1,3,2); bar(nanmean(plotData(sortIdx(1:numel(sortIdx)/2),:))); ylim([0 1])
subplot(1,3,3); bar(nanmean(plotData(sortIdx(numel(sortIdx)/2+1:end),:))); ylim([0 1])

NDTchange = nanmean(HDDM_summary.nondecisionMRI(idxHDDM,2:4),2)-HDDM_summary.nondecisionMRI(idxHDDM,1);

figure; scatter(plotData, NDTchange, 'filled')

%% sort by brainscore

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/B_data/STSWD_summary.mat')

IDs = {'1117';'1118';'1120';'1124';'1125';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';'1215';...
    '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281'};

pn.HDDMdata = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/D_DDM/B_data/';

idxHDDM = ismember(STSWD_summary.IDs,IDs);

[sortVal, sortIdx] = sort(STSWD_summary.brainscoreSummary.DDM_LV1(idxHDDM,5), 'ascend');

figure; 
plotData = squeeze(nanmean(nanmean(RunOnsetMat,4),2));
subplot(1,3,1); imagesc(plotData(sortIdx,:))
subplot(1,3,2); bar(nanmean(plotData(sortIdx(1:numel(sortIdx)/2),:)))
subplot(1,3,3); bar(nanmean(plotData(sortIdx(numel(sortIdx)/2+1:end),:)))
