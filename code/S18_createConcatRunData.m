% create 4-1 SD mats for each individual run

% 180323 | adapted by JQK from Steffen's script

clear all; clc; restoredefaultpath;

pn.root      = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/task/';
pn.tools	 = [pn.root, 'analyses/D_PLS_Dim/T_tools/'];  addpath(genpath(pn.tools));
pn.data      = [pn.root, 'analyses/D_PLS_Dim/B_data/'];
pn.matpath   = [pn.data, 'SD_STSWD_byCond_v5/'];

% N = 44 YA + 51 OA; 2131, 2237 dropped due to missing runs
IDs = {'1117';'1118';'1120';'1124';'1125';'1126';'1131';'1132';'1135';'1136';...
    '1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';'1215';...
    '1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';...
    '1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';...
    '2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';'2149';'2157';...
    '2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';...
    '2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2238';...
    '2241';'2244';'2246';'2248';'2250';'2251';'2252';'2258';'2261'};

%% change the datamat information

for indID = 1:(numel(IDs))
    disp(['Processing subject ', IDs{indID}]);
    for indRun = 1:4
        %% load _BfMRIsessiondata.mats variables to be changed 
        try
          a = load([pn.matpath, 'SD_', IDs{indID}, '_v5_run', num2str(indRun),'_BfMRIsessiondata.mat']);
        catch ME
          disp (ME.message)
        end
        % relChange 4 vs 1
        tmp_st_datamat(indRun,:) = (nanmean(a.st_datamat(4,:),1)-nanmean(a.st_datamat(1,:),1))./nanmean(a.st_datamat(1,:),1);
    end
    b = a;
    b.st_datamat = tmp_st_datamat;
    b.st_datamat(isnan(b.st_datamat)) = 0;
    b.session_info.datamat_prefix = ['SD_', IDs{indID}, '_v5_runsConcat'];
    b.session_info.condition = cell(4,1);
    b.session_info.condition(1,1) = {'Run1_4rel1'};
    b.session_info.condition(2,1) = {'Run2_4rel1'};
    b.session_info.condition(3,1) = {'Run3_4rel1'};
    b.session_info.condition(4,1) = {'Run4_4rel1'};
    b.session_info.num_conditions  = numel(b.session_info.condition);
    b.session_info.num_conditions0 = numel(b.session_info.condition);
    for indCond = size(b.session_info.condition_baseline,2)+1:numel(b.session_info.condition)
       b.session_info.condition_baseline{indCond}  = b.session_info.condition_baseline{1};
       b.session_info.condition_baseline0{indCond} = b.session_info.condition_baseline0{1};
    end
    b.num_subj_cond = repmat(1,1,numel(b.session_info.condition));
    b.st_evt_list   = 1:1:numel(b.session_info.condition);
    save([pn.matpath, b.session_info.datamat_prefix '_BfMRIsessiondata.mat'], '-struct', 'b');
end
